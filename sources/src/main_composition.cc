/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <algorithm>
#include <iterator>
#include <list>
#include <sstream>
using namespace std;

#include <tclap/CmdLine.h>
using namespace TCLAP;

#include <AAAlphabet.h>
#include <Counter.h>
#include <DNAAlphabet.h>
#include <Sequence.h>
#include <SequenceFactory.h>
#include <Word.h>
#include <WordSequence.h>
using namespace rmes;

const int LINEWIDTH=80;


int
main(int argc, char **argv)
{
  CmdLine *cmdLine=new CmdLine(PACKAGE,' ',VERSION);

  ValueArg<string> *seqArg=new ValueArg<string>("s","seq","Sequence filename (FASTA or Genbank format.",true,"","filename");
  cmdLine->add(seqArg);

  ValueArg<short> *hminArg=new ValueArg<short>("i","hmin","Shortest word length.",false,1,"integer");
  cmdLine->add(hminArg);
  ValueArg<short> *hmaxArg=new ValueArg<short>("a","hmax","Longest word length.",false,1,"integer");
  cmdLine->add(hmaxArg);

  ValueArg<short> *lengthArg=new ValueArg<short>("l","length","Word length.",false,1,"integer");
  cmdLine->add(lengthArg);


  SwitchArg *aaArg=new SwitchArg("","aa","Use amino acid alphabet.",false);
  cmdLine->add(aaArg);

  cmdLine->parse(argc,argv);

  short hmin=1;
  short hmax=1;
  if (hminArg->isSet() && hmaxArg->isSet()) {
    if (lengthArg->isSet()) {
      cerr << "Command-line format error : either -i (--hmin) and -a (--hmax) OR -l (--length) can be used." << endl;
      return (EXIT_FAILURE);
    }
    hmin=hminArg->getValue();
    hmax=hmaxArg->getValue();
  }  else {
    if (lengthArg->isSet()) {
      hmin=lengthArg->getValue();
      hmax=lengthArg->getValue();
    }
  }



  if (aaArg->isSet())
    Alphabet::alphabet=new AAAlphabet();

  SequenceFactory seqFact(seqArg->getValue());
  if (seqFact.hasMoreSequences()) {
    Sequence seq=seqFact.nextSequence();
    Counter counter(hmin,hmax);
    counter.countWords(seq);

    cout << endl 
	 << "Sequence identifier:"  << endl 
	 << "\t" << seq.name() << endl
	 << endl
	 << "Sequence length: " << seq.length() << endl
	 << endl << endl;

    for (short l=hmin;l<=hmax;l++) {
      string  line="";
      cout << "Words of length "<< l << ":" << endl;
      WordSequence wseq(l);
      for (WordSequence::iterator it=wseq.begin(); it != wseq.end() ; it++) {
		string fieldstring;
	ostringstream fieldstream(fieldstring);
	fieldstream << *it  << ": " << counter.wordCount(it->length(),
							it->number()) << " ";
	if (line.length()+fieldstream.str().length() >= LINEWIDTH) {
	  cout << line << endl;
	  line="";
	}
	line=line+fieldstream.str();
      }
      cout << line << endl << endl;
    }
    
  }

  delete aaArg;
  delete lengthArg;
  delete hmaxArg;
  delete hminArg;
  delete cmdLine;

  return (EXIT_SUCCESS);
}
