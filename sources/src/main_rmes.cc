/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cassert>
#include <fstream>
#include <memory>
#include <sstream>
using namespace std;

#include <AAAlphabet.h>
#include <Alphabet.h>
#include <Counter.h>
#include <DNAAlphabet.h>
#include <Estimator.h>
#include <EstimatorFactory.h>
#include <RMESParameters.h>
#include <RMESParameterParser.h>
#include <PhasedEstimator.h>
#include <PhasedWord.h>
#include <RMESException.h>
#include <SequenceFactory.h>
#include <Sequence.h>
#include <SkewEstimator.h>
#include <SkewFamilySetEstimator.h>
#include <SkewWordEstimator.h>

using namespace rmes;

#if defined(HAVE_LIBZ) && defined(HAVE_ZLIB_H)
#include <gzstream.h>
#endif

const int PRECISION=6;

int
main(int argc, char **argv)
{

  try {
    RMESParameters params;
    
    RMESParameterParser parser(PACKAGE,VERSION);
    
    parser.parse(argc,argv,&params);


    if (params.AAAlphabet()) {
      delete Alphabet::alphabet;
      Alphabet::alphabet=new AAAlphabet();
    }

    if (!params.AAAlphabet() && !params.DNAAlphabet()) {
      delete Alphabet::alphabet;
      Alphabet::alphabet=new Alphabet(params.alphabetString());
    }
    PhasedWord::PHASES=params.phases();

    auto_ptr<Estimator> e(EstimatorFactory::getEstimator(params));
    
    SequenceFactory seqFact(params.sequenceFile());

    if (seqFact.hasMoreSequences()) {
      Sequence seq=seqFact.nextSequence();
      e->estimate(seq);

      ostream *os=NULL;
      short phase=0;
      do {
	ostringstream filenamestream("");
	if (params.phases()>0) {
	  ((PhasedEstimator *)e.get())->setCurrentPhase(phase);
	  filenamestream << params.outputPrefix() << "." << phase+1;
	} else {
	  if (params.method() == "compare")
	    filenamestream << params.outputPrefix() << ".compare";
	  else
	    filenamestream << params.outputPrefix() << ".0";
	}
	if (params.compressOutput() == true) {
#if defined(HAVE_LIBZ) && defined(HAVE_ZLIB_H)
	  filenamestream <<".gz";
	  os=new ogzstream(filenamestream.str().c_str());
#else
	  throw (RMESException("Compressed output generation not available!"));
#endif
	} else {
	  os=new ofstream(filenamestream.str().c_str());
	}
	*os << *e;
	delete os;
	++phase;
      } while (phase<=params.phases());
   
      if (params.method() == "skew") {
	Estimator *ep=e.get();
	SkewEstimator *se=NULL;
	if (params.familyFile().length()) {
	  SkewFamilySetEstimator *sfe=dynamic_cast<SkewFamilySetEstimator *>(ep);
	  se=dynamic_cast<SkewEstimator *>(sfe);
	} else {
	  SkewWordEstimator *swe=dynamic_cast<SkewWordEstimator *>(ep);
	  se=dynamic_cast<SkewEstimator *>(swe);
	}
	ostringstream filenamestream("");
	filenamestream << params.outputPrefix() << ".0.skew";
	if (params.compressOutput() == true) {
#if defined(HAVE_LIBZ) && defined(HAVE_ZLIB_H)
	  filenamestream <<".gz";
	  os=new ogzstream(filenamestream.str().c_str());
#else
	  throw (RMESException("Compressed output generation not available!"));
#endif
	} else {
	  os=new ofstream(filenamestream.str().c_str());
	}
	*os << *se;
	delete os;
      }
    }

    delete Alphabet::alphabet;

  } catch (RMESException &re) {
    cerr << "***" << re.message << endl;
    return(EXIT_FAILURE);
  }


  return(EXIT_SUCCESS);

}
