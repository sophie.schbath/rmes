/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <algorithm>
#include <iterator>
#include <list>
using namespace std;

#include <tclap/CmdLine.h>
using namespace TCLAP;

#include <DNAAlphabet.h>
using namespace rmes;

int
main(int argc, char **argv)
{
  CmdLine *cmdLine=new CmdLine(PACKAGE,' ',VERSION);

  ValueArg<string> *patternArg=new ValueArg<string>("p","pattern","Pattern used to generate word families.",true,"","string");
  cmdLine->add(patternArg);

  ValueArg<string> *titleArg=new ValueArg<string>("t","title","Title of the resulting family file.",false,"","label");
  cmdLine->add(titleArg);


  cmdLine->parse(argc,argv);

  string title="";
  if (titleArg->isSet())
    title=titleArg->getValue();

  cout << title << " # " << endl;

  string pattern=patternArg->getValue();

  long numwords=1;
  long numfamilies=1;
  for(string::iterator it=pattern.begin();it !=pattern.end(); it++) 
    if (*it == '#')
      numfamilies *=Alphabet::alphabet->size();
    else if (*it == Alphabet::alphabet->joker())
      numwords *=Alphabet::alphabet->size();

  cout << numfamilies << endl << numwords << endl << pattern.length() << endl;

  list<string> families;
  families.push_back(pattern);
  
  bool finished=true;
  do {
    finished=true;
    for(list<string>::iterator it=families.begin(); finished && it!=families.end();
	it++) {
      string pattern=*it;
      for (short i=0; i<pattern.length();i++)
	if (pattern[i]=='#') {
	  finished=false;
	  vector<string> expansions;
	  for (short baseIndex=0;baseIndex<Alphabet::alphabet->size();
	       baseIndex++) {
	    char basechar=Alphabet::alphabet->character(baseIndex);
	    string familyPattern=pattern;
	    familyPattern[i]=basechar;
	    expansions.push_back(familyPattern);
	  }
	  families.insert(it,expansions.begin(),expansions.end());
	  families.erase(it);
	  break;
	}
    }
  } while (!finished);
  
  for(list<string>::iterator it=families.begin(); it!=families.end();
      it++) {
    cout << " " << *it << endl;
    string pattern=*it;
    if (pattern.find(Alphabet::alphabet->joker()) != string::npos) {
      list<string> patternlist;
      patternlist.push_back(pattern);
      bool finished=true;
      do {
	finished=true;
	for(list<string>::iterator it=patternlist.begin(); finished && it!=patternlist.end();
	    it++) {
	  string pattern=*it;
	  for (short i=0; i<pattern.length();i++)
	    if (pattern[i]==Alphabet::alphabet->joker()) {
	      finished=false;
	      vector<string> expansions;
	      for (short baseIndex=0;baseIndex<Alphabet::alphabet->size();
		   baseIndex++) {
		char basechar=Alphabet::alphabet->character(baseIndex);
		string familyPattern=pattern;
		familyPattern[i]=basechar;
		expansions.push_back(familyPattern);
	      }
	      patternlist.insert(it,expansions.begin(),expansions.end());
	      patternlist.erase(it);
	      break;
	    }
	}
      } while (!finished);
      for (list<string>::iterator it=patternlist.begin(); it != patternlist.end(); it++)
	cout << "     " << *it << endl;
    } else {
      cout << "     " << *it << endl;
    }    
  }

  delete patternArg;
  delete titleArg;
  delete cmdLine;

  return (EXIT_SUCCESS);
}
