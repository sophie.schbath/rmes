/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fstream>
#include <memory>
#include <sstream>
using namespace std;

#include <FormatParameters.h>
#include <FormatParameterParser.h>
#include <ResultReader.h>
#include <ResultSet.h>
#include <RMESException.h>
using namespace rmes;


int
main(int argc, char **argv)
{

  try {
    FormatParameters params;
    
    FormatParameterParser parser(PACKAGE,VERSION);
    
    parser.parse(argc,argv,&params);


    ResultReader reader;

    auto_ptr<ResultSet> rs(reader.readResults(cin,params));

    cout << *(rs.get()) << endl;

  } catch (RMESException &re) {
    cerr << "***" << re.message << endl;
    return(EXIT_FAILURE);
  }

  return(EXIT_SUCCESS);

}
