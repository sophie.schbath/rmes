/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <CompoundPoissonFamilyResultSet.h>

#include <algorithm>
#include <iomanip>
using namespace std;

namespace rmes {
	CompoundPoissonFamilyResultSet::CompoundPoissonFamilyResultSet(const float minThresh, const float maxThresh) :
			FamilyResultSet(minThresh,maxThresh,"CompoundPoisson")
	{
		vector<HeaderStruct>::iterator hit=find(_headers.begin(),_headers.end(),HeaderStruct("score",0));
		_headers.insert(hit,HeaderStruct("expect_p",10));
	}

	void
	CompoundPoissonFamilyResultSet::resize(short minSize, short maxSize) {
		FamilyResultSet::resize(minSize,maxSize);
		_expect_p.resize(maxSize+1);
		
	}

	istream &
	CompoundPoissonFamilyResultSet::readParameter(istream &is, const string &h)
	{
		if (h=="expect_p") {
			float expect_p=readFloatValue(is);
			_expect_p[_minSize].push_back(expect_p);	
		} else
			FamilyResultSet::readParameter(is,h);

		return is;
	}	

	
	ostream &
	CompoundPoissonFamilyResultSet::writeParameter(ostream &os, const short s, const HeaderStruct &h, const long index) const
	{
		if (h._name == "expect_p") 
			os << FIELDSEPARATOR << setw(h._width) << right << _expect_p[_minSize][index];
		else
			FamilyResultSet::writeParameter(os,s,h,index);
		return os;
	}

	CompoundPoissonFamilyResultSet::~CompoundPoissonFamilyResultSet()
	{
	}
};
