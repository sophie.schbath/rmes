/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <ResultReader.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
using namespace std;

#include <Alphabet.h>
#include <FormatParameters.h>
#include <ResultSet.h>
#include <ResultSetFactory.h>

namespace rmes {

  const string ResultReader::SEPARATOR="#";
  const string ResultReader::FAMILYSTRING="_familles";

  ResultReader::ResultReader()
  {
  }

  ResultSet *
  ResultReader::readResults(istream &is, const FormatParameters &params) {

    string line;
    getline(is,line);

    if (line[0] == '#') {
      // Process header
      getline(is,line); //Read Alphabet line.
      int eqpos=line.find_first_of("=");
      string alphabetString=line.substr(eqpos+1);
      istringstream alphaStream(alphabetString);
      vector<string> alphaTokens;
      copy(istream_iterator<string>(alphaStream),istream_iterator<string>(),back_inserter(alphaTokens));
      Alphabet *alphabet=new Alphabet(alphaTokens,"__ICHAR__",'%');
      Alphabet::alphabet=alphabet;
      getline(is,line);
    } 

    // Extract data name, method, word/families and phase information.
    int commentpos=line.find_last_of(SEPARATOR);
    string dataSet=line.substr(0,commentpos);
    string rmesInfo=line.substr(commentpos+1,line.length()-commentpos-1);

    istringstream rmesInfoStream(rmesInfo);
    string algorithm;
    short phase;
    
    rmesInfoStream >> algorithm >> phase;

    ResultSetFactory rsFactory;
    ResultSet *rs=rsFactory.getResultSet(is,dataSet,algorithm,phase,params);
    rs->setDataSetName(dataSet);

    is >> *rs;

    return rs;
  }


  ResultSet *
  ResultReader::readResults(const string &s, const FormatParameters &params)
  {
    ifstream ifs(s.c_str());
    ResultSet *rs=readResults(ifs,params);
    ifs.close();
    return rs;
  }

  ResultReader::~ResultReader()
  {
  }
};
