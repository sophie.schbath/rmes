/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <WordSequence.h>

#include <iostream>
using namespace std;

namespace rmes {

  WordSequence::WordSequence(const short l) : _length(l)
  {
  }

  WordSequence::iterator
  WordSequence::begin()
  {
    Word firstWord(_length,0);
    return iterator(this,firstWord);
  }

  WordSequence::iterator
  WordSequence::end()
  {
    Word lastWord(_length+1,0);
    return iterator(this,lastWord);
  }


  WordSequence::iterator::iterator(WordSequence *ws, Word &cw) : _ws(ws), _curWord(cw)
  {
  }

  WordSequence::iterator &
  WordSequence::iterator::operator++()
  {
    bool carry=false;
    short index=0;
    do {
      Base base=_curWord[_curWord.length()-index-1]+1;
      long shiftBits=Alphabet::alphabet->bits()*index;
      if (base == Alphabet::alphabet->size()) {
	base=0;
	carry=true;
	index++;
      } else {
	carry=false;
      }
      long number=_curWord.number();
      long cleanMask=~(Alphabet::alphabet->mask()<<shiftBits);
      number = number & (cleanMask);
      long baseMask=base<<shiftBits;
      number = number | baseMask;
      _curWord.setNumber(number);
    } while (carry && index<_curWord.length());
    if (index==_curWord.length())
      _curWord=Word(_curWord.length()+1,0);
    return *this;
  }

  WordSequence::iterator
  WordSequence::iterator::operator++(int)
  {
    iterator tmp=*this;
    ++*this;
    return tmp;
  }

  Word
  WordSequence::iterator::operator*()
  {
    return _curWord;
  }

  Word *
  WordSequence::iterator::operator->()
  {
    return &_curWord;
  }

  bool
  operator==(const WordSequence::iterator &i1, const WordSequence::iterator &i2)
  {
    return i1._ws==i2._ws && i1._curWord.length()==i2._curWord.length() &&
      i1._curWord.number() == i2._curWord.number();
  }
  
  bool
  operator!=(const WordSequence::iterator &i1, const WordSequence::iterator &i2)
  {
    return i1._ws!=i2._ws || i1._curWord.length()!=i2._curWord.length()
      || i1._curWord.number()!=i2._curWord.number();
  }

};



