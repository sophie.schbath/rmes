/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedWord.h>

#include <memory>

using namespace std;

#include <StatComputations.h>

namespace rmes {

  short PhasedWord::PHASES=3;

  PhasedWord::PhasedWord(const long l, const short p) : 
    Word(l), _phase(p)
  {
  }


  PhasedWord::PhasedWord(const long l, const long n, const short p) :
    Word(l,n), _phase(p)
  {
  }

  PhasedWord::PhasedWord(const string &s, const short p) :
    Word(s), _phase(p)
  {
  }

  PhasedWord &
  PhasedWord::operator=(const PhasedWord &pw)
  {
    
    if (this != &pw) {
      this->Word::operator=(pw);
      _phase=pw._phase;
    }
    return *this;
  }

  PhasedWord::PhasedWord(const Word &w, const short p) :
    Word(w), _phase(p)
  {
  }


  PhasedWord *
  PhasedWord::overlaps(const PhasedWord &pw, const short d) const
  {
    PhasedWord *respw=NULL;

    auto_ptr<Word> ow(this->Word::overlaps(pw,d));
    if (ow.get()) {
      const PhasedWord *leftWord=NULL;
      const PhasedWord *rightWord=NULL;
      short dabs=0;
      if (d>=0) {
	leftWord=this;
	rightWord=&pw;
	dabs=d;
      } else {
	leftWord=&pw;
	rightWord=this;
	dabs=-d;
      }
						
      if (positiveMod((rightWord->length() - rightWord->phase() + dabs -
	   (leftWord->length() - leftWord->phase())),PHASES) == 0) {
	short phase=leftWord->phase();
	if (leftWord->length() - dabs < rightWord->length())
	  phase=rightWord->phase();
	respw=new PhasedWord(*(ow.get()),phase);
      }
    }
    return respw;
    
  }

  PhasedWord::~PhasedWord()
  {
  }


  bool
  operator==(const PhasedWord &pw1, const PhasedWord &pw2)
  {
    const Word *w1=&pw1;
    const Word *w2=&pw2;
    return (*w1==*w2 && pw1.phase() == pw2.phase());
  }

  bool
  operator!=(const PhasedWord &pw1, const PhasedWord &pw2)
  {
    return !(pw1==pw2);
  }
};
