/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedGaussWordEstimator.h>

using namespace std;

#include <StatComputations.h>
#include <WordSequence.h>

namespace rmes {

  PhasedGaussWordEstimator::PhasedGaussWordEstimator(const short lmin,
						     const short lmax,
						     const short m,
						     const short nphases) :
    PhasedWordEstimator(lmin,lmax,m,"Cond_as",nphases)
  {
    _counter=new PhasedCounter(lmin,lmax,nphases);
    _counterPlusOne=new PhasedCounter((m==-1)?(lmin-1):(m+1), (m==-1)?(lmax-1):(m+1),nphases);
  }

  void
  PhasedGaussWordEstimator::estimate(const Sequence &s)
  {
    PhasedWordEstimator::estimate(s);
    _counter->countWords(s);
    _counterPlusOne->countWords(s);
    for (short l=_lmin; l<=_lmax; l++) {
      WordSequence wseq(l);
      for (WordSequence::iterator it=wseq.begin(); it != wseq.end(); ++it) {
	long w=it->number();
        computeExpectation(l,w);
	computeVariance(l,w);
	for (short p=0;p<=_nphases;p++) {
	  _stat[p][l][w]=computeStat(_counter->wordCount(l,w,p),
				     _expect[p][l][w],
				     _var[p][l][w]);
	}
      }
    }
  }

  void
  PhasedGaussWordEstimator::computeExpectation(const short l, const long w)
  {
    _expect[_nphases][l][w]=0.0;
    for (short p=0;p<_nphases;p++) {
      PhasedWord pw(l,w,p);
      _expect[p][l][w]=condAsExpect(pw,markovOrder(l),*_counterPlusOne);
      _expect[_nphases][l][w]+=_expect[p][l][w];
    }
  }

  void
  PhasedGaussWordEstimator::computeVariance(const short l, const long w)
  {
    PhasedCounter wordCounter(markovOrder(l)+1,markovOrder(l)+1,_nphases);
    _var[_nphases][l][w]=0.0;
    for (short p=0;p<_nphases;p++) {
      PhasedWord pw(l,w,p);
      wordCounter.countWords(pw,pw.phase());
      _expect[p][l][w]=condAsExpect(pw,markovOrder(l),*_counterPlusOne);
      _var[p][l][w]=condAsVar(pw,markovOrder(l),*_counterPlusOne,wordCounter,_expect[p][l][w]);
      _var[_nphases][l][w]+=_var[p][l][w];
      for (short subp=0;subp<p;subp++) {
	PhasedWord subpw(l,w,subp);
	_expect[subp][l][w]=condAsExpect(subpw,markovOrder(l),*_counterPlusOne);
	float cov= 2*condAsCoVar(pw,subpw,markovOrder(l),*_counterPlusOne,wordCounter,
					      _expect[p][l][w],_expect[subp][l][w]);
	_var[_nphases][l][w] += 2*condAsCoVar(pw,subpw,markovOrder(l),*_counterPlusOne,wordCounter,
					      _expect[p][l][w],_expect[subp][l][w]);
      }
    }
  }
  
  PhasedGaussWordEstimator::~PhasedGaussWordEstimator()
  {
    delete _counter;
    delete _counterPlusOne;
  }

};
