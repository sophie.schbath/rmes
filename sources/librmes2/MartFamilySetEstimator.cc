/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <MartFamilySetEstimator.h>


#include <algorithm>
#include <cmath>
using namespace std;

#include <StatComputations.h>

namespace rmes {

  MartFamilySetEstimator::MartFamilySetEstimator(const FamilySet &fs) :
    FamilySetEstimator("Mart",fs,-1)
  {
    delete _counter;
    _counter=new Counter(fs.wordLength()-1,fs.wordLength());
  }

  void
  MartFamilySetEstimator::estimate(const Sequence &s)
  {
    FamilySetEstimator::estimate(s);

    long numberOfFamilies=_familySet.numberOfFamilies();
    long wordsPerFamily=_familySet.wordsPerFamily();
    short wordLength=_familySet.wordLength();

    fill(_counts[wordLength].begin(),_counts[wordLength].end(),0);
    fill(_expect[wordLength].begin(),_expect[wordLength].end(),0.0);
    fill(_var[wordLength].begin(),_var[wordLength].end(),0.0);
    fill(_stat[wordLength].begin(),_stat[wordLength].end(),0.0);

    Word baseWord(wordLength);
    Word basePrefix(wordLength-1);
    Word baseSuffix(wordLength-1);
    Word baseRadix(wordLength-2);


    Word otherWord(wordLength);
    Word otherPrefix(wordLength-1);
    Word otherSuffix(wordLength-1);
    Word otherRadix(wordLength-2);

    for (long familyIndex=0; familyIndex<numberOfFamilies; familyIndex++) {
      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	long wordOneNumber=_familySet[familyIndex][wordOneIndex];
	baseWord.setNumber(wordOneNumber);
	long nBaseWord=_counter->wordCount(wordLength,baseWord.number());
	basePrefix.setNumber(baseWord.prefix());
	long nBasePrefix=_counter->wordCount(wordLength-1,basePrefix.number());
	baseSuffix.setNumber(baseWord.suffix());
	long nBaseSuffix=_counter->wordCount(wordLength-1,baseSuffix.number());
	baseRadix.setNumber(basePrefix.suffix());
	long nBaseRadixExtended=0;
	for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	  long baseRadixExtended=baseRadix.number()*Alphabet::alphabet->factor()+letter;
	  nBaseRadixExtended+=_counter->wordCount(wordLength-1,baseRadixExtended);
	}

	_counts[wordLength][familyIndex]+=nBaseWord;
	float baseExpect=(1.0*nBasePrefix*nBaseSuffix)/(1.0*nBaseRadixExtended);
	_expect[wordLength][familyIndex]+=baseExpect;
	_var[wordLength][familyIndex]+=baseExpect/(1.0*nBaseRadixExtended*nBaseRadixExtended)*
	  (nBaseRadixExtended-nBaseSuffix)*(nBaseRadixExtended-nBasePrefix);

      }

      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	long wordOneNumber=_familySet[familyIndex][wordOneIndex];
	baseWord.setNumber(wordOneNumber);
	long nBaseWord=_counter->wordCount(wordLength,baseWord.number());
	basePrefix.setNumber(baseWord.prefix());
	long nBasePrefix=_counter->wordCount(wordLength-1,basePrefix.number());
	baseSuffix.setNumber(baseWord.suffix());
	long nBaseSuffix=_counter->wordCount(wordLength-1,baseSuffix.number());
	baseRadix.setNumber(basePrefix.suffix());
	long nBaseRadixExtended=0;
	for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	  long baseRadixExtended=baseRadix.number()*Alphabet::alphabet->factor()+letter;
	  nBaseRadixExtended+=_counter->wordCount(wordLength-1,baseRadixExtended);
	}

	float baseExpect=(1.0*nBasePrefix*nBaseSuffix)/(1.0*nBaseRadixExtended);

	for (long wordTwoIndex=0;wordTwoIndex<wordsPerFamily;wordTwoIndex++) {
	  if (wordOneIndex != wordTwoIndex) {
	    long wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	    otherWord.setNumber(wordTwoNumber);
	    long nOtherWord=_counter->wordCount(wordLength,otherWord.number());
	    otherPrefix.setNumber(otherWord.prefix());
	    long nOtherPrefix=_counter->wordCount(wordLength-1,otherPrefix.number());
	    otherSuffix.setNumber(otherWord.suffix());
	    long nOtherSuffix=_counter->wordCount(wordLength-1,otherSuffix.number());
	    otherRadix.setNumber(otherPrefix.suffix());
	    long nOtherRadixExtended=0;
	    for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	      long otherRadixExtended=otherRadix.number()*Alphabet::alphabet->factor()+letter;
	      nOtherRadixExtended+=_counter->wordCount(wordLength-1,otherRadixExtended);
	    }
	    float otherExpect=(1.0*nOtherPrefix*nOtherSuffix)/(1.0*nOtherRadixExtended);
	    float sumterms=0.0;
	    if (baseRadix == otherRadix)
	      sumterms=sumterms+1.0/nBaseRadixExtended;
	    if (basePrefix == otherPrefix)
	      sumterms=sumterms-1.0/nBasePrefix;
	    if (baseSuffix == otherSuffix)
	      sumterms=sumterms-1.0/nBaseSuffix;
	    float cmax=baseExpect*otherExpect*sumterms;
	    _var[wordLength][familyIndex]+=cmax;
	  }
	}
      }
      _stat[wordLength][familyIndex]=(_counts[wordLength][familyIndex]-_expect[wordLength][familyIndex])/
	sqrt(_var[wordLength][familyIndex]);

    }

  }


  MartFamilySetEstimator::~MartFamilySetEstimator()
  {
  }
};
