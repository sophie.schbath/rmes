/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <CompoundPoissonFamilySetEstimator.h>

#include <algorithm>
#include <cmath>
#include <iterator>
#include <limits>
#include <memory>
#include <numeric>
using namespace std;

typedef vector<short> shortvec;
typedef vector<shortvec> shortmat;

typedef vector<double> doublevec;

#include <StatComputations.h>

namespace rmes {

  CompoundPoissonFamilySetEstimator::CompoundPoissonFamilySetEstimator(
							const FamilySet &fs,
							const short m) :
    FamilySetEstimator("Cond_as_pc",fs,m)
  {
  }

  void
  CompoundPoissonFamilySetEstimator::estimate(const Sequence &s)
  {
    FamilySetEstimator::estimate(s);

    short wordLength=_familySet.wordLength();
    long numberOfFamilies=_familySet.numberOfFamilies();
    long wordsPerFamily=_familySet.wordsPerFamily();

    fill(_counts[wordLength].begin(),_counts[wordLength].end(),0);
    fill(_expect[wordLength].begin(),_expect[wordLength].end(),0.0);
    fill(_var[wordLength].begin(),_var[wordLength].end(),0.0);
    fill(_stat[wordLength].begin(),_stat[wordLength].end(),0.0);

    vector<shortmat> periodpp(wordsPerFamily);
    for(vector<shortmat>::iterator mit=periodpp.begin();
	mit != periodpp.end(); mit++) {
      (*mit).resize(wordsPerFamily);
      for (vector<shortvec>::iterator vit=(*mit).begin();
	   vit != (*mit).end(); vit++)
	(*vit).resize(wordLength-1);
    }

    vector<double> espmots(wordsPerFamily);

    vector<doublevec> A(wordsPerFamily);
    for(vector<doublevec>::iterator it=A.begin();
	it != A.end(); it++) 
      (*it).resize(wordsPerFamily);

    Word wordOne(wordLength);
    Word wordTwo(wordLength);
    Word wordThree(wordLength);
    long wordOneNumber=-1;
    long wordTwoNumber=-1;
    long wordThreeNumber=-1;
    for (long familyIndex=0;familyIndex<numberOfFamilies;familyIndex++) {

      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	wordOneNumber=_familySet[familyIndex][wordOneIndex];
	wordOne.setNumber(wordOneNumber);
	_counts[wordLength][familyIndex]+=_counter->wordCount(wordLength,
							      wordOneNumber);
	espmots[wordOneIndex]=condAsExpect(wordOne,markovOrder(wordLength),
					   *_counterPlusOne);
	_expect[wordLength][familyIndex]+=espmots[wordOneIndex];

	for (long wordTwoIndex=0; wordTwoIndex<wordsPerFamily;
	     wordTwoIndex++) {
	  wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	  wordTwo.setNumber(wordTwoNumber);
	  for (short d=1; d < wordLength; d++) {
	    periodpp[wordOneIndex][wordTwoIndex][d-1]=0;
	    auto_ptr<Word> overlapWord(wordOne.overlaps(wordTwo,d));
	    if (overlapWord.get()) {
	      bool fail=false;
	      if (d>1) {
		for (short wordThreeIndex=0; wordThreeIndex<wordsPerFamily;
		     wordThreeIndex++) {
		  wordThreeNumber=_familySet[familyIndex][wordThreeIndex];
		  wordThree.setNumber(wordThreeNumber);
		  for (short d2=1; d2<d; d2++) {
		    auto_ptr<Word> overlapWordTwo(wordOne.overlaps(wordThree,d2));
		    auto_ptr<Word> overlapWordThree(wordThree.overlaps(wordTwo,d-d2));
		    if (overlapWordTwo.get() && overlapWordThree.get()) {
		      fail=true;
		      break;
		    }
		  }
		}
	      }
	      if (!fail) 
		periodpp[wordOneIndex][wordTwoIndex][d-1]=1;
	      
	    }
	    
	  }
	}
      }

      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	wordOneNumber=_familySet[familyIndex][wordOneIndex];
	wordOne.setNumber(wordOneNumber);
	for (long wordTwoIndex=0; wordTwoIndex<wordsPerFamily;wordTwoIndex++) {
	  A[wordOneIndex][wordTwoIndex]=0.0;
	  short maux=markovOrder(wordLength);
	  if (!maux)
	    maux=1;
	  for (short d=1; d<=wordLength-maux;d++) {
	    if (periodpp[wordOneIndex][wordTwoIndex][d-1]) {
	      wordThreeNumber=wordOne.substring(0,markovOrder(wordLength)+
						     d-1);
	      Word wordFour(markovOrder(wordLength)+d,wordThreeNumber);
	      A[wordOneIndex][wordTwoIndex] += condAsExpect(wordFour,
						    markovOrder(wordLength),
							    *_counterPlusOne);
						     
	    }
	  }
	  if (markovOrder(wordLength) == 0)
	    A[wordOneIndex][wordTwoIndex] /= double(s.length());
	  else {
	    wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	    wordTwo.setNumber(wordTwoNumber);
	    wordThreeNumber=wordTwo.substring(0,markovOrder(wordLength)-1);
	    double denominator=0.0;
	    for (short baseIndex=0;baseIndex<Alphabet::alphabet->size();baseIndex++)
	      denominator += _counterPlusOne->wordCount(
						     markovOrder(wordLength)+1,
				     Alphabet::alphabet->factor()*wordThreeNumber+baseIndex);

	    A[wordOneIndex][wordTwoIndex] /= denominator;
	  }

	}
      }
 
      vector<vecdouble> ImoinsA(wordsPerFamily);
      for (vector<vecdouble>::iterator it=ImoinsA.begin();
	   it != ImoinsA.end(); it++)
	(*it).resize(wordsPerFamily);
      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++)
	for (long wordTwoIndex=0; wordTwoIndex<wordsPerFamily;wordTwoIndex++)
	  if (wordOneIndex==wordTwoIndex)
	    ImoinsA[wordOneIndex][wordTwoIndex] =
	      1 - A[wordOneIndex][wordTwoIndex];
	  else
	    ImoinsA[wordOneIndex][wordTwoIndex] = 
	      - A[wordOneIndex][wordTwoIndex];
	    
      vector<double> aux(wordsPerFamily);
      computeProduct(&ImoinsA,&espmots,&aux);
      double lambda=accumulate(aux.begin(),aux.end(),0.0);
      _var[wordLength][familyIndex]=lambda;
      
      long xmax=_counts[wordLength][familyIndex];
      vector<double> lambdak(1+xmax);
      vector<double> auxbis(wordsPerFamily);
      computeProduct(&ImoinsA,&aux,&auxbis);
      copy(auxbis.begin(),auxbis.end(),aux.begin());
      for(vector<double>::iterator it=lambdak.begin()+1;
	  it != lambdak.end(); it++) {
	*it = accumulate(aux.begin(),aux.end(),0.0);
	computeProduct(&A,&aux,&auxbis);
	copy(auxbis.begin(),auxbis.end(),aux.begin());
      }

      vector<vecdouble> D(1+xmax);
      for (vector<vecdouble>::iterator it=D.begin(); it != D.end(); it++)
	it->resize(1+xmax);
      for (long x=0; x<=xmax; x++)
	for (long y=0; y<=xmax; y++)
	  if (x!=y || x==0)
	    D[x][y]=0;
	  else
	    D[x][x] = exp(-lambda)/double(x);

      for (long y=1; y<=xmax; y++)
	for (long x=y+1; x<=xmax; x++) {
	  double sum=0.0;
	  for (long k=1; k <= x-y; k++)
	    sum += lambdak[k] * D[x-k][y] * double(k)/double(x);
	  D[x][y]=sum;
	}
      
      vector<double> loi(xmax+1);
      loi[0]=exp(-lambda);
      for (long x=1; x<=xmax; x++) {
	double sum=0.0;
	for (long k=1; k<=x; k++) 
	  sum += k * lambdak[k] * D[x][k];
	loi[x] = sum; 

	//if(familyIndex==28021) cout <<"For "<< _familySet[familyIndex].name()<<" NONColored loi["<<x<<"] = "<< loi[x]<<"\n";
      }	

      double proba=0.0;
      short sign=1;
      
      if (_counts[wordLength][familyIndex] <= 
	  _expect[wordLength][familyIndex]) {
	proba=0.0;
	sign = -1;
	sort(loi.begin(),loi.end());
	if (loi.back() <= numeric_limits<double>::min()) {
	  cerr << "Family " << _familySet[familyIndex].name() 
	       << " has a p-value too close to 1." << endl
	       << "Hence, it is significantly rare." << endl;
	  _stat[wordLength][familyIndex]=-1.0*
	    numeric_limits<double>::infinity();
	} else {
	  for (vector<double>::iterator it=loi.begin();
	       it != loi.end(); it++)
	    if (*it <= 1)
	      proba+=*it;
	    else {
	      cerr << "Warning: p-value > 1 for " 
		   << _familySet[familyIndex].name()
		   << ": it is however a very under-represented family." 
		   << endl;
	      break;
	    }
	}
      }
      else {
	proba=1.0;
	sign=+1;
	
	loi.pop_back();
	sort(loi.begin(),loi.end());
	for (vector<double>::reverse_iterator rit=loi.rbegin();
	     rit != loi.rend(); rit++){
	    
	    if (proba - *rit >= 0){
	      proba -= *rit;
	      //if(familyIndex==28021) cout <<"For "<<_familySet[familyIndex].name()<<"proba = "<<proba<<"\n";
	    }
	  else {
	    cerr << "Warning: p-value < 0 for "
		 << _familySet[familyIndex].name()
		 << ": it is however a very over-represented family."
		 << endl;
	    break;
	  }
	}
      }

      if (proba != numeric_limits<double>::infinity()) 
	_stat[wordLength][familyIndex] = sign * quantile(proba);
      else
	_stat[wordLength][familyIndex] = sign * 
	  numeric_limits<double>::infinity();
    }
  }


  void 
  CompoundPoissonFamilySetEstimator::computeProduct(vector<vecdouble> *A, 
						    vector<double> *v,
						    vector<double> *res)
  {
    fill (res->begin(),res->end(),0.0);
    vector<vecdouble>::iterator ALineit=A->begin();
    for (vector<double>::iterator rit=res->begin();
	 rit != res->end();
	 rit++, ALineit++)
      for (vector<double>::iterator AColit=ALineit->begin(),
	     vit=v->begin();
	   AColit != ALineit->end(); 
	   AColit++,
	     vit++)
	*rit += (*AColit)*(*vit);
  }

  CompoundPoissonFamilySetEstimator::~CompoundPoissonFamilySetEstimator()
  {
  }

};
