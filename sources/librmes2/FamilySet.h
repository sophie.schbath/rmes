/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_FAMILYSET_H
#define RMES_FAMILYSET_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <vector>

#include <Family.h>

namespace rmes {
  class FamilySet   {

  public :
    FamilySet(const std::string &, const long, const long, const short);
    virtual ~FamilySet();

    inline std::string name() const;
    inline long numberOfFamilies() const;
    inline long wordsPerFamily() const;
    inline short wordLength() const;
    inline Family & operator[](const long);
    inline void addFamily(const Family &);
    inline std::string fileName() const;

    friend std::ostream & operator<<(std::ostream &, const FamilySet &);
    friend std::istream & operator>>(std::istream &, FamilySet &);

    friend class FamilySetFactory;

  private:
    FamilySet();

    bool readOK();

    std::string _name;
    long _nFamilies;
    long _wordsPerFamily;
    short _wordLength;

    std::vector<Family> _families;

    std::string _fileName;
  };


  inline std::string
  FamilySet::name() const
  {
    return _name;
  }

  inline std::string
  FamilySet::fileName() const
  {
    return _fileName;
  }

  inline long
  FamilySet::numberOfFamilies() const
  {
    return _nFamilies;
  }

  inline long
  FamilySet::wordsPerFamily() const
  {
    return _wordsPerFamily;
  }

  inline short
  FamilySet::wordLength() const
  {
    return _wordLength;
  }


  inline Family &
  FamilySet::operator[](const long i)
  {
    return _families[i];
  }

  inline void
  FamilySet::addFamily(const Family &f)
  {
    _families.push_back(f);
    _nFamilies++;
  }

};

#endif
