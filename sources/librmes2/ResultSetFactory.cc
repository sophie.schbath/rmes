/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <ResultSetFactory.h>

using namespace std;

#include <CompoundPoissonFamilyResultSet.h>
#include <CompoundPoissonWordResultSet.h>
#include <GaussFamilyResultSet.h>
#include <GaussWordResultSet.h>
#include <PoissonWordResultSet.h>
#include <RMESException.h>

namespace rmes  {


  ResultSetFactory::ResultSetFactory()
  {
  }

  ResultSet *
  ResultSetFactory::getResultSet(istream & is,
				 const string &dataset,
				 const string &algorithm,
				 const short &phase,
				 const FormatParameters &params)
  {
   
    ResultSet *rs=NULL;

    if (algorithm == "Cond_as" || algorithm == "Mart") {
      rs=new GaussWordResultSet(params.minLength(),
				params.maxLength(),
				params.minThreshold(),
				params.maxThreshold());
    }    
    if (algorithm == "Cond_as_p") {
      rs=new PoissonWordResultSet(params.minLength(),
				params.maxLength(),
				params.minThreshold(),
				params.maxThreshold());
    }    
    if (algorithm == "Cond_as_pc") {
      rs=new CompoundPoissonWordResultSet(params.minLength(),
				params.maxLength(),
				params.minThreshold(),
				params.maxThreshold());
    }    
    if (algorithm == "Cond_as_familles" || algorithm == "Mart_familles") {
	rs=new GaussFamilyResultSet(params.minThreshold(),params.maxThreshold());
    }
    if (algorithm == "Cond_as_pc_familles") {
	rs=new CompoundPoissonFamilyResultSet(params.minThreshold(),params.maxThreshold());
    }

    if (rs==NULL) {
      throw RMESException("Unrecognized Result Set. Formatting cannot be performed.");
    }

    return rs;
  }


  ResultSetFactory::~ResultSetFactory()
  {
  }

};
