/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <cassert>
#include <iterator>
using namespace std;


#include <Family.h>




#include <Word.h>
namespace rmes {

  Family::Family(const long length, const short wordLength) : _name(""),
						 _wordLength(wordLength)
  {
    _wordNumbers.resize(length);
  }

  Family::Family() : _name(""), _wordLength(0)
  {
  }

  Family::~Family()
  {
  }

  istream &
  operator>>(istream &is, Family &f)
  {
    is >> f._name;
    //    assert (f._pattern.size() == f._wordLength);
    Word word(f._wordLength);
    for (int i=0; i<f._wordNumbers.size(); i++) {
      is >> word;
      f._wordNumbers[i]=word.number();
    }
    return is;
  }
  
  
  ostream &
  operator<<(ostream &os, const Family &f)
  {
    os << " " << f._name << endl;
    Word word(f._wordLength);

    for (int i=0; i<f._wordNumbers.size(); i++) {
      word.setNumber(f._wordNumbers[i]);
      os << "\t" << word << endl;
    }
    return os;
  }
};
