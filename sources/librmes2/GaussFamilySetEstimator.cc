/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <GaussFamilySetEstimator.h>

#include <algorithm>
using namespace std;

#include <StatComputations.h>

namespace rmes {

  GaussFamilySetEstimator::GaussFamilySetEstimator(const FamilySet &fs,
						   const short m) :
    FamilySetEstimator("Cond_as",fs,m)
  {
  }

  void
  GaussFamilySetEstimator::estimate(const Sequence &s)
  {
    FamilySetEstimator::estimate(s);

    long numberOfFamilies=_familySet.numberOfFamilies();
    long wordsPerFamily=_familySet.wordsPerFamily();
    short wordLength=_familySet.wordLength();

    fill(_counts[wordLength].begin(),_counts[wordLength].end(),0);
    fill(_expect[wordLength].begin(),_expect[wordLength].end(),0.0);
    fill(_var[wordLength].begin(),_var[wordLength].end(),0.0);
    fill(_stat[wordLength].begin(),_stat[wordLength].end(),0.0);

    Word wordOne(wordLength);
    Word wordTwo(wordLength);

    for (long familyIndex=0; familyIndex<numberOfFamilies; familyIndex++) {
      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	long wordOneNumber=_familySet[familyIndex][wordOneIndex];
	wordOne.setNumber(wordOneNumber);
	_counts[wordLength][familyIndex]+=_counter->wordCount(wordLength,
							      wordOneNumber);
	double expect=condAsExpect(wordOne,markovOrder(wordLength),
				   *_counterPlusOne);
	_expect[wordLength][familyIndex]+=expect;
	_var[wordLength][familyIndex]+=condAsVar(wordOne,
						 markovOrder(wordLength),
						 *_counterPlusOne,
						 expect);
	
	for (long wordTwoIndex=wordOneIndex+1; wordTwoIndex<wordsPerFamily;
	     wordTwoIndex++) {
	  long wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	  wordTwo.setNumber(wordTwoNumber);
	  	  _var[wordLength][familyIndex]+= 2.0*condAsCoVar(wordOne,
								  wordTwo,
						  markovOrder(wordLength),
							  *_counterPlusOne);
	}


      }
      _stat[wordLength][familyIndex]=computeStat(_counts[wordLength][familyIndex],_expect[wordLength][familyIndex],_var[wordLength][familyIndex]);
    }
  }


  GaussFamilySetEstimator::~GaussFamilySetEstimator()
  {
  }
};
