/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PoissonWordEstimator.h>

#include <algorithm>
using namespace std;

#include <StatComputations.h>
#include <WordClumpCounter.h>
#include <WordSequence.h>

namespace rmes {

  PoissonWordEstimator::PoissonWordEstimator(const short lmin,
					     const short lmax,
					     const short m) :
    WordEstimator(lmin,lmax,m,"Cond_as_p")
  {
    _counter=new WordClumpCounter(lmin,lmax);
    _counterPlusOne=new Counter((m==-1)?(lmin-1) : (m+1),
				(m==-1)?(lmax-1) : (m+1));
  }


  void
  PoissonWordEstimator::estimate(const Sequence &s)
  {

    WordEstimator::estimate(s);
    _counter->countWords(s);

    _counterPlusOne->countWords(s);
    

    for (short l=_lmin; l<=_lmax;l++) {
      WordSequence ws(l);
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	long number=it->number();
	_expect[l][number]=condAsExpect(*it,markovOrder(l),*_counterPlusOne);
	short d0=0;
	for (short d=1; d < l ; d++) {
	  Word *overlapWord=it->overlaps(d);
	  if (overlapWord != NULL) {
	    d0=d;
	    _expect[l][number] -= condAsExpect(*overlapWord,markovOrder(l),
					       *_counterPlusOne);
	    delete overlapWord;
	    break;
	  }
	}
	if (d0) {
	  for (short d=l-d0; d<l; d++) {
	    if (d%d0) {
	      Word *overlapWord=it->overlaps(d);
	      if (overlapWord != NULL) {
		_expect[l][number] -= condAsExpect(*overlapWord,markovOrder(l),
						   *_counterPlusOne);
		delete overlapWord;
	      }
	    }
	  }
	}
	_stat[l][number]= computeStatPoisson(_counter->wordCount(l,number),
						_expect[l][number]);
	_var[l][number]=_expect[l][number];
      }
    }
    
    
  }

  ostream &
  PoissonWordEstimator::writeAlphaToStream(ostream &os) 
  {
    return os;
  }

  PoissonWordEstimator::~PoissonWordEstimator()
  {
    delete _counterPlusOne;
    delete _counter;
  }
					    

};
