/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <SkewFamilySetEstimator.h>

#include <algorithm>
#include <cmath>
#include <iomanip>
using namespace std;

#include <StatComputations.h>

namespace rmes {

  SkewFamilySetEstimator::SkewFamilySetEstimator(const FamilySet &fs,
						   const short m) :
    GaussFamilySetEstimator(fs,m), SkewEstimator()
  {
    _conjcounts.resize(fs.wordLength()+1);
    _conjcounts[fs.wordLength()].resize(fs.numberOfFamilies());
    _score.resize(fs.wordLength()+1);
    _score[fs.wordLength()].resize(fs.numberOfFamilies());
    _skew.resize(fs.wordLength()+1);
    _skew[fs.wordLength()].resize(fs.numberOfFamilies());
    
  }

  void
  SkewFamilySetEstimator::estimate(const Sequence &s)
  {
    GaussFamilySetEstimator::estimate(s);

    long numberOfFamilies=_familySet.numberOfFamilies();
    long wordsPerFamily=_familySet.wordsPerFamily();
    short wordLength=_familySet.wordLength();

    fill(_conjcounts[wordLength].begin(),_conjcounts[wordLength].end(),0);
    fill(_skew[wordLength].begin(),_skew[wordLength].end(),1.0);
    fill(_score[wordLength].begin(),_score[wordLength].end(),0.0);

    Word wordOne(wordLength);
    Word wordOneConj(wordLength);
    Word wordTwo(wordLength);
    Word wordTwoConj(wordLength);



    vector<vecdouble> conjexpect;
    conjexpect.resize(_familySet.wordLength()+1);
    conjexpect[wordLength].resize(_familySet.numberOfFamilies());
    fill(conjexpect[wordLength].begin(),conjexpect[wordLength].end(),0.0);

    vector<vecdouble> conjvar;
    conjvar.resize(_familySet.wordLength()+1);
    conjvar[wordLength].resize(_familySet.numberOfFamilies());
    fill(conjvar[wordLength].begin(),conjvar[wordLength].end(),0.0);

    vector<vecdouble> conjcovar;
    conjcovar.resize(_familySet.wordLength()+1);
    conjcovar[wordLength].resize(_familySet.numberOfFamilies());
    fill(conjcovar[wordLength].begin(),conjcovar[wordLength].end(),0.0);

    for (long familyIndex=0; familyIndex<numberOfFamilies; familyIndex++) {
      for (long wordOneIndex=0; wordOneIndex<wordsPerFamily;wordOneIndex++) {
	long wordOneNumber=_familySet[familyIndex][wordOneIndex];
	wordOne.setNumber(wordOneNumber);
	wordOneConj=wordOne.conjugate();
	_conjcounts[wordLength][familyIndex]+=_counter->wordCount(wordLength,wordOneConj.number());
	double expect=condAsExpect(wordOneConj,markovOrder(wordLength),*_counterPlusOne);
	conjexpect[wordLength][familyIndex]+=expect;
	  conjvar[wordLength][familyIndex]+=condAsVar(wordOneConj,markovOrder(wordLength),*_counterPlusOne,expect);
	for (long wordTwoIndex=0; wordTwoIndex<wordsPerFamily; wordTwoIndex++) {
	  long wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	  wordTwo.setNumber(wordTwoNumber);
	  wordTwoConj=wordTwo.conjugate();
	  if (wordTwoIndex>wordOneIndex)
	    conjvar[wordLength][familyIndex]+= 2.0*condAsCoVar(wordOneConj,
							       wordTwoConj,
							       markovOrder(wordLength),
							       *_counterPlusOne);
	  conjcovar[wordLength][familyIndex]+=condAsCoVar(wordOne,wordOneConj,markovOrder(wordLength),*_counterPlusOne);
	}
      }
      _skew[wordLength][familyIndex]=(1.0*_counts[wordLength][familyIndex])/(1.0*_conjcounts[wordLength][familyIndex]);
      double numerator=_expect[wordLength][familyIndex]-_skew[wordLength][familyIndex]*conjexpect[wordLength][familyIndex];
      double denom=_var[wordLength][familyIndex]
	+_skew[wordLength][familyIndex]*_skew[wordLength][familyIndex]*conjvar[wordLength][familyIndex]
	-2*_skew[wordLength][familyIndex]*conjcovar[wordLength][familyIndex];
      _score[wordLength][familyIndex]=-numerator/sqrt(denom);
    }
  }

  ostream &
  SkewFamilySetEstimator::writeSkewToStream(ostream &os)
  {
    Estimator::writeBlockToStream(os);
    os 	 << "-------------------------------------------------------" << endl
	 << setw(min(7,_familySet.wordLength()+1)) << left << "Family" 
	 << setw(10) << right << "Count"
	 << setw(13) << right << "Conj. Count" 
	 << setw(14) << right << "Skew" 
	 << setw(14) << right << "Score" << endl
	 << "-------------------------------------------------------" << endl;
    
    for (long familyIndex=0; familyIndex < _familySet.numberOfFamilies() ;
	 familyIndex++) {
      os << setw(min(7,_familySet.wordLength()+1)) << left << _familySet[familyIndex].name()
	 << setw(10) << right <<  _counts[_familySet.wordLength()][familyIndex] 
	 << setw(13) << right << _conjcounts[_familySet.wordLength()][familyIndex]
	 << setw(14) << right << scientific << setprecision(6) << _skew[_familySet.wordLength()][familyIndex] 
	 << setw(14) << right << scientific << setprecision(6) << _score[_familySet.wordLength()][familyIndex] << endl;
    }
    return os;
  }

  SkewFamilySetEstimator::~SkewFamilySetEstimator()
  {
  }
};
