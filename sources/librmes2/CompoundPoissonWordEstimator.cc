/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <CompoundPoissonWordEstimator.h>

#include <cmath>
#include <limits>
#include <memory>
using namespace std;


#include <StatComputations.h>
#include <WordSequence.h>

namespace rmes  {

  CompoundPoissonWordEstimator::CompoundPoissonWordEstimator(const short lmin,
							     const short lmax,
							     const short m) :
    WordEstimator(lmin,lmax,m,"Cond_as_pc")
  {
    _fact.resize(lmax+1);
    for (short l=_lmin;l<=_lmax;l++)
      _fact[l].resize(1<<(Alphabet::alphabet->bits()*l));
    
    _counter=new Counter(lmin,lmax);
    _counterPlusOne=new Counter((m==-1)?(lmin-1):(m+1),
				(m==-1)?(lmax-1):(m+1));
  }
  
  
  void
  CompoundPoissonWordEstimator::estimate(const Sequence &s)
  {
    WordEstimator::estimate(s);
    
    _counter->countWords(s);
    _counterPlusOne->countWords(s);
    
    long maxCount=0;
    for (short l=_lmin;l<=_lmax;l++) 
      maxCount=(_counter->maxCount(l) > maxCount) ? _counter->maxCount(l) : maxCount;

    for (short l=_lmin; l<=_lmax; l++) {
      WordSequence ws(l);
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	long wordIndex=it->number();
	double proba=0.0;
	double fac_am=0.0;
	short sign=1;

	double expect = condAsExpect(*it,markovOrder(l),*_counterPlusOne);
	_expect[l][wordIndex]=expect;
	if (expect == 0.0) {
	  _var[l][wordIndex]=0.0;
	  _fact[l][wordIndex]=1.0;
	  _stat[l][wordIndex]=0.0;
	  
	} else {
	  short overlap=0;
	  double expect_p = condAsPoissonExpect(*it,markovOrder(l),*_counterPlusOne,
						expect,&overlap);
	  _var[l][wordIndex]=expect_p;

	  /* Calcul du facteur a_m */
	  if (overlap > l-markovOrder(l) || overlap==l) {
	    fac_am=0.0;
	  } else {
	    /* nouveau calcul a partir du 23/08/07*/
	    if (expect!=0.0)
	      fac_am = 1.0-(expect_p/expect);
	    else
	      fac_am = 1.0 ;
	  }
	  /* Fin du calcul du facteur a_m */

	  double lambda1 = (( 1.0 - fac_am) * (1.0 - fac_am))*_expect[l][wordIndex];
	  if (fac_am == 0.0) {
	    /* Poisson approximation */
	    if (_counter->wordCount(l,wordIndex) <= _expect[l][wordIndex])
	      _stat[l][wordIndex]=computeStatPoisson(_counter->wordCount(l,wordIndex),
						     _expect[l][wordIndex]);
	    else
	      _stat[l][wordIndex]=computeStatPoisson(_counter->wordCount(l,wordIndex)-1,
						     _expect[l][wordIndex]);

	    if (_stat[l][wordIndex] == numeric_limits<double>::infinity()) {
	      cerr << "Warning in CompoundPoisson estimation. Statistic of word " << *it 
		   << " is infinite." << endl;
	    }
	  } else {
	    /* Compound Poisson approximation */
	    long maxCount = _counter->wordCount(l,wordIndex);
	    vector<double> loi(maxCount+1);
	    vector<double> log_fact(maxCount+1);
	    log_fact[0] = 0.0;
	    double aux=log((1.0-fac_am)*(1.0-fac_am)*expect/double(fac_am));
	    loi.resize(maxCount+1);
	    fill(loi.begin(),loi.end(),0.0);
	    loi[0]=exp(lambda1/(fac_am-1.0));
	    for (long x=1; x<=maxCount; x++) {
	      log_fact[x] = log_fact[x-1] + log(double(x));
	      for (long y=1; y <= x; y++) {
		loi[x] += exp(double(x)*log(fac_am)
			      + log_fact[x-1] - log_fact[y]
			      - log_fact[y-1] - log_fact[x-y]
			      + double(y)*aux + (fac_am-1.0)*expect);
	      }
	    }
	    if (_counter->wordCount(l,wordIndex) <= _expect[l][wordIndex]) {
	      /* Computation of the lower tail */
	      sign=-1;

	    //	    /* ancien algo */
// 	    sort(loi.begin(),loi.end());
// 	    
// 	    proba=0.0;
// 	    if (*(loi.rbegin()) <= numeric_limits<double>::min()) {
// 	      cerr << "Word " << *it << " has a p-value too close to 1 " 
// 		   << "(stat = -infinity)" << endl << "Hence, it is significantly rare." << endl;
// 	      _stat[l][wordIndex]=-1.0*numeric_limits<double>::infinity();
// 	    } else {
// 	      /* j'ai l'impression qu'il manque le dernier terme*/
// 	      for(vector<double>::iterator pit=loi.begin(); pit != loi.end(); pit++) {
// 		if (proba + *pit  <=1.0)
// 		  proba+=*pit;
// 		else {
// 		  cerr << "Warning: p-value > 1 for " << *it
// 		       << ": it is however a very under-represented word." << endl;
// 		  break;
// 		}
// 	      }
// 	    }

	      /* nouvel algo */
	      proba= lower_tail(expect_p,fac_am,maxCount) ;
	    } 
	    else {
	    /* Computation of the upper tail */
	      sign=1;

// 	    /* ancien algo */
// 	    loi.pop_back();
// 	    sort(loi.begin(),loi.end());
// 	    proba=1.0;
// 	    for (vector<double>::reverse_iterator rit=loi.rbegin();
// 		 rit != loi.rend(); rit++) 
// 	      if (proba - *rit >=0.0)
// 		proba -= *rit;
// 	      else {
// 		cerr << "Warning: p-value < 0 for " << *it
// 		     << ": it is however a very over-represented word." << endl;
// 		break;
// 	      }

	    /* nouvel algo */
	      proba= upper_tail(expect_p,fac_am,maxCount) ;
	      
	    }
	    if (proba != numeric_limits<double>::infinity()) {
	      _stat[l][wordIndex]=sign*quantile(proba);
	    } else {
	      _stat[l][wordIndex]=numeric_limits<double>::infinity();
	    }
	  }
	  _fact[l][wordIndex]=fac_am;
	}
      }
    }

  }
    
  double
  CompoundPoissonWordEstimator::condAsPoissonExpect(const Word &w,
						     const short m, const Counter &c,
						     const double expect, short *overlap)
  {
    double retval=expect;
    *overlap=w.length();

    for (short d=1; d< w.length(); d++) {
      auto_ptr<Word> overlapWord(w.overlaps(d));
      if (overlapWord.get()) {

	*overlap=d;
	if (d<=(w.length()-m)) /* So: ajout du 23/08/07*/
	  retval -= condAsExpect(*overlapWord.get(),m,c);
	
	break;
      }
    }

    if (*overlap < w.length()) {
      // for (short d=w.length() - *overlap ; d < w.length(); d++) {
      for (short d=w.length() - *overlap ; d <= w.length()-m ; d++) {
	if (d % *overlap) {
	  auto_ptr<Word> overlapWord(w.overlaps(d));
	  if (overlapWord.get())
	    retval -= condAsExpect(*overlapWord.get(),m,c);
	}
      }
    }

    return retval;
  }

  ostream &
  CompoundPoissonWordEstimator::writeBlockToStream(ostream &os) 
  {
    Estimator::writeBlockToStream(os);
    os   << _name << " # " << _algorithm << " " << 0 << endl;
    static const int DEFAULTPRECISION=6;
    os.setf(ios::fixed,ios::floatfield);
    os << _lmin << " " << _lmax << endl;
    for (short l=_lmin;l<=_lmax;l++) {
      os << endl << markovOrder(l) << " " <<  markovOrder(l) << endl;
      WordSequence ws(l);
      long columnIndex=-1;
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _counter->wordCount(l,it->number());
      }
      os << endl;

      os.precision(3);
      columnIndex=-1;
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _expect[l][it->number()];
      }
      os << endl;

      os.precision(3);
      columnIndex=-1;
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _var[l][it->number()];
      }
      os << endl;


      os.precision(6);
      columnIndex=-1;
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _fact[l][it->number()];
      }
      os << endl;

      os.precision(4);
      columnIndex=-1;
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _stat[l][it->number()];
      }
      os << endl;
      os.precision(DEFAULTPRECISION);
    }
    return os;
  }

  ostream &
  CompoundPoissonWordEstimator::writeAlphaToStream(ostream &os) 
  {
    return os;
  }

  CompoundPoissonWordEstimator::~CompoundPoissonWordEstimator()
  {
    delete _counter;
    delete _counterPlusOne;
  }
  
};
