/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <FamilySet.h>

#include <algorithm>
#include <iterator>
using namespace std;

namespace rmes {
  
  FamilySet::FamilySet(const string &name, const long nFamilies, 
		       const long wordsPerFamily, const short wordLength) :
    _name(name), _nFamilies(nFamilies), 
    _wordsPerFamily(wordsPerFamily), _wordLength(wordLength)
  {
  }

  FamilySet::FamilySet() : _name(""), _nFamilies(0), 
			   _wordsPerFamily(0), _wordLength(0)
  {
  }

  bool
  FamilySet::readOK() {
    return (_nFamilies>0 && _wordsPerFamily>0 && _wordLength>0);
  }

  FamilySet::~FamilySet()
  {
  }

  ostream &
  operator<<(ostream &os, const FamilySet &fs)
  {
    os << fs._name << endl
       << fs._nFamilies << endl
       << fs._wordsPerFamily << endl
       << fs._wordLength << endl;
    
    copy(fs._families.begin(),fs._families.end(),
	 ostream_iterator<Family>(os));
    
    return os;
  }
  
  istream &
  operator>>(istream &is, FamilySet &fs)
  {
    fs._name="";
    fs._nFamilies=0;
    fs._wordsPerFamily=0;
    fs._wordLength=0;
    fs._families.clear();

    getline(is,fs._name);
    is >> fs._nFamilies 
       >> fs._wordsPerFamily 
       >> fs._wordLength;

    if (fs._nFamilies > 0 && fs._wordsPerFamily > 0 && fs._wordLength > 0) {
      for (int i=0; i<fs._nFamilies; i++) {
	Family family(fs._wordsPerFamily,fs._wordLength);
	is >> family;
	fs._families.push_back(family);
      }
    }

    return is;
    
  }
};

