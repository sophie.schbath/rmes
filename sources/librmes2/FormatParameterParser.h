/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_FORMATPARAMETERPARSER_H
#define RMES_FORMATPARAMETERPARSER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>
#include <vector>

#include <tclap/CmdLine.h>


#include <RMESException.h>
#include <FormatParameters.h>

namespace rmes {

  struct FormatParameterParserException : public RMESException {
    FormatParameterParserException(const std::string &m) : 
      RMESException("Argument parsing exception: "+m) {};
  };


  class FormatParameterParser  {

  public:
    FormatParameterParser(const std::string &, const std::string &);

    virtual ~FormatParameterParser();


    virtual void parse(int, char **, FormatParameters *);


  protected:

    TCLAP::CmdLine *_cmdLine;

    TCLAP::ValueArg<short> *_minLengthArg;
    TCLAP::ValueArg<short> *_maxLengthArg;
    TCLAP::ValueArg<short> *_wordLengthArg;

    TCLAP::ValueArg<float> *_minThreshArg;
    TCLAP::ValueArg<float> *_maxThreshArg;

  };
  
};
#endif
