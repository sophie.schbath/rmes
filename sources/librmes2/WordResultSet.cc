/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <Word.h>
#include <WordResultSet.h>
#include <WordSequence.h>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iterator>
#include <limits>
#include <list>

#include <ios>
#include <iomanip>
#include <iostream>
using namespace std;

namespace rmes {

  WordResultSet::WordResultSet(const short minSize, const short maxSize,
			       const float minThresh, const float maxThresh, const string &algorithm) :
    ResultSet(minSize,maxSize,minThresh,maxThresh,algorithm)
  {
    _headers.insert(_headers.begin(),HeaderStruct("word",10));
  }

  void
  WordResultSet::resize(const short minSize, const short maxSize)
  {
    _minSize=minSize;
    _maxSize=maxSize;
    _counts.resize(maxSize+1);
    _expect.resize(maxSize+1);
    _stat.resize(maxSize+1);
    _markovOrders.resize(maxSize+1);
    _words.resize(maxSize+1);
    for (short size=_minSize; size<=_maxSize; size++) {
	    _counts[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
	    _expect[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
	    _stat[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
    }
  }

  ostream &
  WordResultSet::writeParameter(ostream &os, const short s,
				const HeaderStruct &h, const long index) const
  {
    if (h._name=="word")
      os << setw(h._width) << right << Word(s,_words[s][index]);
    else
	    ResultSet::writeParameter(os,s,h,index);

    return os;
  }

  istream &
  WordResultSet::readDataBlock(istream &is, const short s, const string &h)
  {
	  
    if (s<_minSize || s>_maxSize ) {
      float ignore;
      for (long index=0; index<pow((float)Alphabet::alphabet->size(),s);index++)
	is >> ignore;
    } 
    else {
      if (h=="word") {
	      WordSequence ws(s);
	      for (WordSequence::iterator wit=ws.begin(); wit!=ws.end(); wit++)
		      _words[s].push_back(wit->number());
      }

      if (h == "count")
	for (long index=0; index<_counts[s].size();index++) 
	  is >> _counts[s][index];
      if (h == "expect")
	for (long index=0; index<_expect[s].size();index++)
	  _expect[s][index]=readFloatValue(is);
      if (h == "score")
	for (long index=0; index<_stat[s].size();index++) {
	  IndexedStats idxs;
	  idxs._index=index;
	  idxs._stat=readFloatValue(is);
	  _stat[s][index]=idxs;
	}
    }
    return is;
  }

  istream &
  WordResultSet::readFromStream(istream &is)
  {
    short minFileSize, maxFileSize;
    
    is >> minFileSize >> maxFileSize;
   if (_minSize<0 || _maxSize<_minSize) {
   	_minSize=minFileSize;
	_maxSize=maxFileSize;
   }
	  resize(_minSize,_maxSize);
   
    for (short size=minFileSize; size <=maxFileSize; size++) {
      is >> _markovOrders[size] >> _markovOrders[size];

      for (vector<HeaderStruct>::iterator it=_headers.begin();
	   it != _headers.end(); it++) 
		readDataBlock(is,size,it->_name);
      
		
      if (size>=_minSize && size <=_maxSize)
	      sort(_stat[size].begin(),_stat[size].end(),CompareIndexedStats);
    }
    return is;
  }

  WordResultSet::~WordResultSet()
  {
  }

}
