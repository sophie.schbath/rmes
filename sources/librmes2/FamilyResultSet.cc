/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <FamilyResultSet.h>

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <limits>
using namespace std;

namespace rmes {

	FamilyResultSet::FamilyResultSet(const float minThresh, const float maxThresh, 
				const string &algorithm) :  
		ResultSet(-1, -1, minThresh, maxThresh, algorithm+" Families")
	{
		_headers.insert(_headers.begin(),HeaderStruct("family",10));
	}

  	void
  	FamilyResultSet::resize(const short minSize, const short maxSize)
  	{
    		_minSize=minSize;
    		_maxSize=maxSize;
    		_counts.resize(maxSize+1);
    		_expect.resize(maxSize+1);
    		_stat.resize(maxSize+1);
    		_markovOrders.resize(maxSize+1);

  	}

	istream &
	FamilyResultSet::readParameter(istream &is, const string &h) 
	{
	  string numberString;

	  if (h == "family") {
	    string fname;
	    is >> fname;
	    _familyNames.push_back(fname);
	  }
	  
	  if (h == "count") {
	    long count;
	    is >> count;
	    _counts[_minSize].push_back(count);
	  }
	  
	  if (h== "expect") {
	    float expect=readFloatValue(is);
	    _expect[_minSize].push_back(expect);
	  }
	  
	  if (h== "score") {
	    float stat=readFloatValue(is);
	    long index=_stat[_minSize].size();
	    _stat[_minSize].push_back(IndexedStats(index,stat));
	  }
	  return is;				
	}

	istream &
	FamilyResultSet::readFromStream(istream &is)
	{
		short ignore; 
		
		is >> _minSize >> _maxSize;
		is >> _nFamilies;

		is >> ignore; // Ignore number of words per family.

		is >> _familySetName;
		_algorithm=_algorithm+" ("+_familySetName+")";

		is >> ignore; // Ignore max number of chars of family labels.

		resize(_minSize,_maxSize);
		
		is >> _markovOrders[_minSize]>> _markovOrders[_minSize];
		

		for (long i=0;i<_nFamilies;i++)
			for (vector<HeaderStruct>::iterator hit=_headers.begin(); hit != _headers.end(); hit++)
				readParameter(is,hit->_name);	

		sort(_stat[_minSize].begin(),_stat[_minSize].end(),CompareIndexedStats);
			
	}

	ostream &
	FamilyResultSet::writeParameter(ostream &os, const short s, const HeaderStruct &h, const long index) const
	{
		if (h._name=="family")
			os << setw(h._width) << right << _familyNames[index]; 
		else
			ResultSet::writeParameter(os,s,h,index);
		return os;
	}

	FamilyResultSet::~FamilyResultSet()
	{
	}
};
