/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedMartFamilySetEstimator.h>


#include <iostream>
#include <algorithm>
#include <cmath>
#include <memory>
using namespace std;

#include <StatComputations.h>

namespace rmes {

  PhasedMartFamilySetEstimator::PhasedMartFamilySetEstimator(const FamilySet &fs, 
                               const short nphases) : PhasedFamilySetEstimator("Mart",fs,-1,nphases)
  {
    
  }

  

  void
  PhasedMartFamilySetEstimator::estimate(const Sequence &s)
  {
    
    PhasedFamilySetEstimator::estimate(s);
    long numberOfFamilies=_familySet.numberOfFamilies();
    long wordsPerFamily=_familySet.wordsPerFamily();
    short wordLength=_familySet.wordLength();

    Word wordOne(wordLength);
    Word prefixOne(wordLength-1);
    Word suffixOne(wordLength-1);
    Word radixOne(wordLength-2);
 
    Word wordTwo(wordLength);
    Word prefixTwo(wordLength-1);
    Word suffixTwo(wordLength-1);
    Word radixTwo(wordLength-2);

    for (long familyIndex=0; familyIndex<numberOfFamilies;familyIndex++) {

      for (short p=0;p<_nphases;p++) {

	_counts[p][wordLength][familyIndex]=0;
	_expect[p][wordLength][familyIndex]=0.0;
	_var[p][wordLength][familyIndex]=0.0;

	for (long wordOneIndex=0; wordOneIndex < wordsPerFamily; wordOneIndex++) {
	  
	  long wordOneNumber=_familySet[familyIndex][wordOneIndex];
	  wordOne.setNumber(wordOneNumber); 
	  long nWordOne=_counter->wordCount(wordLength,wordOneNumber,p);
	  prefixOne.setNumber(wordOne.prefix());
	  long nPrefixOne=_counterPlusOne->wordCount(wordLength-1,prefixOne.number(),positiveMod(p-1,_nphases));
	  suffixOne.setNumber(wordOne.suffix());
	  long nSuffixOne=_counterPlusOne->wordCount(wordLength-1,suffixOne.number(),p);
	  radixOne.setNumber(prefixOne.suffix());
	  long nRadixOneExtended=0;
	  for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	    long RadixOneExtended=radixOne.number()*Alphabet::alphabet->factor()+letter;
	    nRadixOneExtended += _counterPlusOne->wordCount(wordLength-1,RadixOneExtended,p);
	  }

	  _counts[p][wordLength][familyIndex] += nWordOne;
	  
	  double expect=0.0;
	  if (nRadixOneExtended!=0)
	    expect=(1.0*nPrefixOne*nSuffixOne)/(1.0*nRadixOneExtended);
	  _expect[p][wordLength][familyIndex] += expect;

	  double var=0.0;
	  if (nRadixOneExtended!=0)
	      var=expect/(1.0*nRadixOneExtended*nRadixOneExtended)*
		(nRadixOneExtended-nSuffixOne)*(nRadixOneExtended-nPrefixOne);	
	  if (var<0.0)
	    var=0.0;		
	  _var[p][wordLength][familyIndex] += var;
	  _var[_nphases][wordLength][familyIndex] += var;

	  for (long wordTwoIndex=0;wordTwoIndex<wordOneIndex;wordTwoIndex++) {

	    long wordTwoNumber=_familySet[familyIndex][wordTwoIndex];
	    wordTwo.setNumber(wordTwoNumber); 
	    long nWordTwo=_counter->wordCount(wordLength,wordTwoNumber,p);
	    prefixTwo.setNumber(wordTwo.prefix());
	    long nPrefixTwo=_counterPlusOne->wordCount(wordLength-1,prefixTwo.number(),positiveMod(p-1,_nphases));
	    suffixTwo.setNumber(wordTwo.suffix());
	    long nSuffixTwo=_counterPlusOne->wordCount(wordLength-1,suffixTwo.number(),p);
	    radixTwo.setNumber(prefixTwo.suffix());
	    long nRadixTwoExtended=0;
	    for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	      long RadixTwoExtended=radixTwo.number()*Alphabet::alphabet->factor()+letter;
	      nRadixTwoExtended+=_counterPlusOne->wordCount(wordLength-1,RadixTwoExtended,p);
	    }
	  
	    double covar=0.0;
	    if ((nRadixOneExtended!=0)&&(radixOne==radixTwo))
	    {
	      covar += nPrefixTwo*nSuffixTwo;
	      if (prefixOne==prefixTwo)
		covar -= nRadixOneExtended*nSuffixTwo;
	      if (suffixOne==suffixTwo)
		covar -= nRadixOneExtended*nPrefixTwo;
	      covar *= expect/(1.0*nRadixOneExtended*nRadixOneExtended);
	    }
	    _var[p][wordLength][familyIndex] += 2*covar;
	  }
	}
      }

      _counts[_nphases][wordLength][familyIndex]=0;
      _expect[_nphases][wordLength][familyIndex]=0.0;
      _var[_nphases][wordLength][familyIndex]=0.0;
      for (short p=0; p<_nphases;p++){
	  _counts[_nphases][wordLength][familyIndex] += _counts[p][wordLength][familyIndex];
	  _expect[_nphases][wordLength][familyIndex] += _expect[p][wordLength][familyIndex];
	  _var[_nphases][wordLength][familyIndex] += _var[p][wordLength][familyIndex];
      }

      for (short p=0; p<=_nphases;p++){
	_stat[p][wordLength][familyIndex]=computeStat(_counts[p][wordLength][familyIndex],
						      _expect[p][wordLength][familyIndex],
						      _var[p][wordLength][familyIndex]);
      }
    }
  }
 

   PhasedMartFamilySetEstimator::~PhasedMartFamilySetEstimator()
  {
  }
};
