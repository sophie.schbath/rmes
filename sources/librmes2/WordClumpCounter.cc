/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

typedef vector<long> longvect;

#include <WordClumpCounter.h>
#include <Alphabet.h>
#include <Word.h>
namespace rmes {

  WordClumpCounter::WordClumpCounter(const long lmin, const long lmax) : 
    Counter(lmin,lmax)
  {
  }
  
  void
  WordClumpCounter::countWords(const String &s)
  {
    
    /*
     * prevoccend holds the ending position of all word occurences,
     * for each word length, 
     * and is needed to detect overlapping occurrences.
     * Initialized with a size equal to the total number of words
     * for the maximal word length.
     */
    vector<longvect> prevoccend(_lmax+1);
    /*
     * Fill vector of preivous word occurrence ends with initial value (-1).
     */
    
    
    for (short l=_lmin; l<=_lmax;l++) {
      fill(_counts[l].begin(),_counts[l].end(),0);
      prevoccend[l].resize(numWords(l));
      fill(prevoccend[l].begin(),prevoccend[l].end(),-1);
    }

    for (long start=0; start<s.length() -_lmax+1;start++) {
      for (short l=_lmin; l<=_lmax; l++) {
	long wordnumber=s.substring(start,start+l-1);
	/**
	 * Check if word does not overlap another occurrence of itself,
	 * before increasing its occurence counter.
	 */
	if (wordnumber>=0) {
	  if (start>prevoccend[l][wordnumber])
	    _counts[l][wordnumber]++;
	  prevoccend[l][wordnumber]=start+l-1;
	}
      }
    }
    if (s.end() > s.length() - _lmax) {
      short lmax_ok = _lmax -1;
      for (long start=s.length() - _lmax+1; start <= s.end(); start++) {
	for (short l=_lmin; l <= lmax_ok; l++) {
	  long wordnumber=s.substring(start,start+l-1);
	  if (wordnumber >=0) {
	    if (start>prevoccend[l][wordnumber])
	      _counts[l][wordnumber++];
	    prevoccend[l][wordnumber]=start+l-1;
	  }
	}
	lmax_ok--;
      }
    }
  }

  WordClumpCounter::~WordClumpCounter()
  {
  }

  ostream &
  operator<<(ostream &os, const WordClumpCounter &c)
  {
    return os;
  }
};

