/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_WORDESTIMATOR_H
#define RMES_WORDESTIMATOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <Counter.h>
#include <SimpleEstimator.h>
#include <MarkovEstimator.h>
#include <Sequence.h>
#include <Word.h>

namespace rmes {

  class WordEstimator : public SimpleEstimator, 
			public MarkovEstimator {
  public :

    virtual ~WordEstimator();

    virtual void estimate(const Sequence &);
    virtual void countWords(const Sequence &);

    virtual void setOutputMode(const OutputMode);

    virtual short minLength() const;
    virtual short maxLength() const;
    
    virtual long wordCount(const short, const long);
    virtual long numWords(const short);


    virtual std::ostream &writeBlockToStream(std::ostream &);

  protected :
    WordEstimator(const short, const short, const short,
		  const std::string &);

    virtual long *wordCounts(const short);

    /**
     * Computes the expecation of a subword of this Word, given the
     * word counts of the relevant word sizes.
     *
     * Before calling this method, it is required that the wordCount()
     * method has already been invoked on the Counter instance
     * passed as argument.
     *
     * \param l  the length of the subword for which the expectation
     * is to be computed.
     * \param c the Counter instance containing the wordcounts on 
     * which the expectation computation is based. 
     * \return the expectation of the subword of length w.
     *
     **/
    virtual double condAsExpect(const Word &w, const short m, const Counter &c);

    /**
     * Computes the variance of a subword of this Word, given the word
     * counts of the relevant word sizes.
     *
     * Before calling this method, it is required that the wordCount()
     * method has already been invoked on the Counter instance
     * passed as argument.
     *
     * \param l  the length of the subword for which the variance
     * is to be computed.
     * \param c the Counter instance containing the wordcounts on 
     * which the variance computation is based. 
     * \return the variance of the subword of length w.
     *
     **/
    virtual double condAsVar(const Word &w, const short l, const Counter &c, const double expect);

    /**
     * Computes the covariance between  a subword of this Word, and a subword
     * of the word given as argument, given the word counts
     * of the relevant word sizes.
     *
     * Before calling this method, it is required that the wordCount()
     * method has already been invoked on the Counter instance
     * passed as argument.
     *
     * \param w Word used for covariance computation.
     * \param m  the length of the subword for which the covariance
     * is to be computed.
     * \param c the Counter instance containing the wordcounts on 
     * which the covariance computation is based. 
     * \return the covariance of the subword of length w.
     *
     **/
    virtual double condAsCoVar(const Word & w1, const Word &w2,
			       const short l, 
    			       const Counter &c);


    short _lmin;
    short _lmax;

    Counter *_counter;

  private :
    WordEstimator();
    WordEstimator(const WordEstimator &);
    WordEstimator &operator=(const WordEstimator &);

  };
};


#endif
