/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <SkewWordEstimator.h>

#include <algorithm>
#include <cmath>
#include <iomanip>
#include <limits>
using namespace std;

#include <ResultSet.h>
#include <StatComputations.h>
#include <Word.h>
#include <WordSequence.h>
namespace rmes {
  
  SkewWordEstimator::SkewWordEstimator(const short lmin, const short lmax, 
					 const short m) : 
    GaussWordEstimator(lmin, lmax, m), SkewEstimator()
  {
    _skew.resize(lmax+1);
    _score.resize(lmax+1);
    for (short l=lmin;l<=lmax;l++) {
      _skew[l].resize(1<<Alphabet::alphabet->bits()*l);
      _score[l].resize(1<<Alphabet::alphabet->bits()*l);
    }
  }


  void
  SkewWordEstimator::estimate(const Sequence &s)
  {
    this->GaussWordEstimator::estimate(s);
    
    for (int l=_lmin; l<=_lmax; l++) {
      fill(_skew[l].begin(),_skew[l].end(),1.0);
      fill(_score[l].begin(),_score[l].end(),0.0);
      WordSequence  ws(l);
      for (WordSequence::iterator it=ws.begin();  it != ws.end(); it++) {
	long number=it->number();
	Word conjugate=it->conjugate();
	long cnumber=conjugate.number();
	if (number != cnumber && _skew[l][number]==1.0) {
	  int occnumber=_counter->wordCount(l,number);
	  int coccnumber=_counter->wordCount(l,cnumber);
	  if (occnumber != 0 || coccnumber != 0) {
	    if (coccnumber>occnumber) {
	      double b=(1.0*occnumber)/(1.0*coccnumber);
	      _skew[l][number]=b;
	      _skew[l][cnumber]=1.0/b;
	      _score[l][number]=-1.0*(_expect[l][number]-b*_expect[l][cnumber])/
		sqrt(_var[l][number]+b*b*_var[l][cnumber]-2*b*condAsCoVar(*it,conjugate,markovOrder(l),*_counterPlusOne));
	      _score[l][cnumber]=-_score[l][number];
	    } else {
	      double cb=(1.0*coccnumber)/(1.0*occnumber);
	      _skew[l][cnumber]=cb;
	      _skew[l][number]=1.0/cb;
	      _score[l][cnumber]=-1.0*(_expect[l][cnumber]-cb*_expect[l][number])/
		sqrt(_var[l][cnumber]+cb*cb*_var[l][number]-2*cb*condAsCoVar(conjugate,*it,markovOrder(l),*_counterPlusOne));
	      _score[l][number]=-_score[l][cnumber];
	    }
	  }
	}
      }
    }
  }

  ostream &
  SkewWordEstimator::writeSkewToStream(ostream &os)
  {
    Estimator::writeBlockToStream(os);
    for (short l=_lmin;l<=_lmax;l++) {
      os << "Skew for words of length " << l << endl
	 << "-------------------------------------------------------" << endl
	 << setw(min(5,l+1)) << left << "Word" 
	 << setw(10) << right << "Count"
	 << setw(13) << right << "Conj. Count" 
	 << setw(14) << right << "Skew" 
	 << setw(14) << right << "Score" << endl
	 << "-------------------------------------------------------" << endl;

      WordSequence ws(l);
      vector<IndexedStats> skewstats;
      for (WordSequence::iterator it=ws.begin(); it!=ws.end(); it++) {
	skewstats.push_back(IndexedStats(it->number(),_score[l][it->number()]));
      }
      sort(skewstats.begin(),skewstats.end(),CompareIndexedStats);
      for (vector<IndexedStats>::reverse_iterator skewstat=skewstats.rbegin();
	   skewstat!=skewstats.rend();skewstat++)
      {
	Word *w=new Word(l,skewstat->_index);
	long number=skewstat->_index;
	Word cword=w->conjugate();
	long cnumber=cword.number();
	os << setw(min(5,l+1)) << left << *w
	   << setw(10) << right << _counter->wordCount(l,number) 
	   << setw(13) << right << _counter->wordCount(l,cnumber) 
	   << setw(14) << right << scientific << setprecision(6) <<_skew[l][number]    
	   << setw(14) << right << scientific << setprecision(6) << _score[l][number] << endl;
	delete w;
      }
    }
    return os;
  }

  ostream &
  SkewWordEstimator::writeAlphaToStream(ostream &os)
  {
    return os;
  }

  SkewWordEstimator::~SkewWordEstimator()
  {
  }
};

