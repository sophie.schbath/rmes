/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_RMESPARAMETERPARSER_H
#define RMES_RMESPARAMETERPARSER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>
#include <vector>

#include <tclap/CmdLine.h>


#include <RMESException.h>
#include <RMESParameters.h>

namespace rmes {

  struct RMESParameterParserException : public RMESException {
    RMESParameterParserException(const std::string &m) : 
      RMESException("Argument parsing exception: "+m) {};
  };


  class RMESParameterParser  {

  public:
    RMESParameterParser(const std::string &, const std::string &);

    virtual ~RMESParameterParser();


    virtual void parse(int, char **, RMESParameters *);


  protected:

    TCLAP::CmdLine *_cmdLine;

    TCLAP::SwitchArg *_gaussMethodArg;
    TCLAP::SwitchArg *_poissonMethodArg;
    TCLAP::SwitchArg *_compoundPoissonMethodArg;
    TCLAP::SwitchArg *_skewMethodArg;
    TCLAP::SwitchArg *_compareMethodArg;

    TCLAP::ValueArg<short> *_markovOrderArg;
    
    TCLAP::ValueArg<short> *_minLengthArg;
    TCLAP::ValueArg<short> *_maxLengthArg;
    TCLAP::ValueArg<short> *_wordLengthArg;

    TCLAP::ValueArg<std::string> *_outputPrefixArg;
    TCLAP::ValueArg<std::string> *_sequenceFileArg;
    TCLAP::ValueArg<std::string> *_familyFileArg;
    
    TCLAP::SwitchArg *_maxOrderArg;

    TCLAP::SwitchArg *_dnaAlphabetArg;
    TCLAP::SwitchArg *_aaAlphabetArg;
    TCLAP::ValueArg<std::string> *_alphabetStringArg;
#ifdef HAVE_ZLIB_H
    TCLAP::SwitchArg *_compressOutputArg;
#endif

    TCLAP::ValueArg<short> *_phasesArg;

    TCLAP::ValueArg<std::string> *_refCompareSequenceFileArg;
    TCLAP::ValueArg<std::string> *_compareSequenceFileArg;

  };
  
};
#endif
