/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <AAAlphabet.h>

#include <string>
using namespace std;

namespace rmes {


  vector<string> AAAlphabet::ResidueChars = 
                                          AAAlphabet::buildResidueVector();

  string AAAlphabet::InterruptChars("xX");

  char AAAlphabet::JokerChar('$');

  AAAlphabet::AAAlphabet() : Alphabet(AAAlphabet::ResidueChars,
				      AAAlphabet::InterruptChars,
				      AAAlphabet::JokerChar)
  {
  }

  AAAlphabet::~AAAlphabet()
  {
  }
  
  vector<string>
  AAAlphabet::buildResidueVector()
  {
    vector<string> resvec;
    resvec.push_back("aA");
    //resvec.push_back("bB");
    resvec.push_back("cC");
    resvec.push_back("dD");
    resvec.push_back("eE");
    resvec.push_back("fF");
    resvec.push_back("gG");
    resvec.push_back("hH");
    resvec.push_back("iI");
    resvec.push_back("kK");
    resvec.push_back("lL");
    resvec.push_back("mM");
    resvec.push_back("nN");
    resvec.push_back("oO");
    resvec.push_back("pP");
    resvec.push_back("qQ");
    resvec.push_back("rR");
    resvec.push_back("sS");
    resvec.push_back("tT");
    resvec.push_back("uU");
    resvec.push_back("vV");
    resvec.push_back("wW");
    resvec.push_back("yY");
    //resvec.push_back("zZ");

    return resvec;
  }

};

