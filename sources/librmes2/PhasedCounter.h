/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_PHASEDCOUNTER_H
#define RMES_PHASEDCOUNTER_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vector>

#include <Counter.h>
#include <PhasedWord.h>

typedef std::vector<long> longvect;
typedef std::vector<longvect> longmat;
typedef std::vector<short> shortvect;
typedef std::vector<shortvect> shortmat;


namespace rmes {

  class PhasedCounter : public Counter {
  public :
    PhasedCounter(const short lmin, const short lmax, const short nphases);
    virtual ~PhasedCounter();

    virtual void countWords(const String &, const short=-1);
    virtual long *wordCounts(const short l, const short p);
    virtual long wordCount(const short l, const long w, const short p) const;
    virtual long maxCount(const short l, const short p);
    virtual long minCount(const short l, const short p);
    virtual long numWords(const short l) const;

  protected:
    mutable std::vector<longmat> _phasedCounts;
    std::vector<longvect> _phasedMaxcounts;
    std::vector<longvect> _phasedMincounts;
    short _nphases;
  };



};


#endif
