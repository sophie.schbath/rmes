/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedGaussFamilySetEstimator.h>


using namespace std;

#include <StatComputations.h>

namespace rmes {

  PhasedGaussFamilySetEstimator::PhasedGaussFamilySetEstimator(const FamilySet &fs, const short m,
							       const short nphases) : PhasedFamilySetEstimator("Cond_as",fs,m,nphases)
  {
    _wordExpect.resize(nphases+1);
    for (short p=0;p<=nphases;p++) {
      _wordExpect[p].resize(fs.wordLength()+1);
      _wordExpect[p][fs.wordLength()].resize(1<<(Alphabet::alphabet->bits()*fs.wordLength()));
    }
  }
  
  void
  PhasedGaussFamilySetEstimator::computeExpect(const long index, FamilySet &fs)
  {
    long wordLength=fs.wordLength();
    long wordsPerFamily=fs.wordsPerFamily();
    
    _counts[_nphases][wordLength][index]=0;
    _expect[_nphases][wordLength][index]=0.0;
    fill (_wordExpect[_nphases][fs.wordLength()].begin(),_wordExpect[_nphases][fs.wordLength()].end(),0.0);
    
    for (short p=0;p<_nphases;p++) {
      _counts[p][wordLength][index]=0;
      _expect[p][wordLength][index]=0.0;
      for (long wordIndex=0; wordIndex < wordsPerFamily; wordIndex++) {
	long wordNumber=fs[index][wordIndex];
	PhasedWord pw(wordLength,wordNumber,p);
	_counts[p][wordLength][index] += _counter->wordCount(wordLength,wordNumber,p);
	_counts[_nphases][wordLength][index] += _counter->wordCount(wordLength,wordNumber,p);
	
	double expect=condAsExpect(pw,markovOrder(wordLength),*_counterPlusOne);
	_wordExpect[p][wordLength][wordNumber]=expect;
	_wordExpect[_nphases][wordLength][wordNumber] +=expect;
	_expect[p][wordLength][index] += expect;
	_expect[_nphases][wordLength][index] += expect;
      }
      
    }
  }
  
  void
  PhasedGaussFamilySetEstimator::computeVar(const long index, FamilySet &fs)
  {
    long wordLength=fs.wordLength();
    long wordsPerFamily=fs.wordsPerFamily();
    
    PhasedCounter wordCounter(markovOrder(wordLength)+1,markovOrder(wordLength)+1,_nphases);
    _var[_nphases][wordLength][index]=0.0;
    
    for (short p=0;p<_nphases;p++) {
      _var[p][wordLength][index]=0.0;
      for (long wordIndex=0; wordIndex<wordsPerFamily;wordIndex++) {
	long wordNumber=fs[index][wordIndex];
	PhasedWord pword(wordLength,wordNumber,p);
	wordCounter.countWords(pword,pword.phase());
	double wordVar=condAsVar(pword,markovOrder(wordLength),*_counterPlusOne,wordCounter,
				 _wordExpect[p][wordLength][wordNumber]);
	_var[p][wordLength][index]+=wordVar;
	_var[_nphases][wordLength][index]+=wordVar;
	
	for (long word2Index=wordIndex+1; word2Index<wordsPerFamily; word2Index++) {
	  long word2Number=fs[index][word2Index];
	  PhasedWord pword2(wordLength,word2Number,p);
	  _var[p][wordLength][index] += 2.0 *
	    condAsCoVar(pword,pword2,markovOrder(wordLength),
			*_counterPlusOne,wordCounter,
			_wordExpect[p][wordLength][wordNumber],
			_wordExpect[p][wordLength][word2Number]);
	  for (short p2=0; p2<_nphases;p2++) {
	    PhasedWord pword3(wordLength,word2Number,p2);
	    _var[_nphases][wordLength][index] += 2.0 *
	      condAsCoVar(pword,pword3,markovOrder(wordLength),
			  *_counterPlusOne,wordCounter,
			  _wordExpect[p][wordLength][wordNumber],
			  _wordExpect[p2][wordLength][word2Number]);
	    
	  }
	}
	for (short p2=p+1;p2<_nphases;p2++) {
	  PhasedWord pword2(wordLength,wordNumber,p2);
	  _var[_nphases][wordLength][index] += 2.0 *
	    condAsCoVar(pword,pword2,markovOrder(wordLength),
			*_counterPlusOne,wordCounter,
			_wordExpect[p][wordLength][wordNumber],
			_wordExpect[p2][wordLength][wordNumber]);
	}
	
      }
    }
  }
  
  void
  PhasedGaussFamilySetEstimator::estimate(const Sequence &s)
  {
    PhasedFamilySetEstimator::estimate(s);
    long numberOfFamilies=_familySet.numberOfFamilies();
    short wordLength=_familySet.wordLength();
    for (long familyIndex=0; familyIndex<numberOfFamilies;familyIndex++) {
      computeExpect(familyIndex,_familySet);
      computeVar(familyIndex,_familySet);
      for (short p=0; p<=_nphases;p++)
	_stat[p][wordLength][familyIndex]=computeStat(_counts[p][wordLength][familyIndex],
						      _expect[p][wordLength][familyIndex],_var[p][wordLength][familyIndex]);
    }
  }
  
  PhasedGaussFamilySetEstimator::~PhasedGaussFamilySetEstimator()
  {
  }
};
