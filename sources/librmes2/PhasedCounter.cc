/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedCounter.h>

#include <algorithm>

using namespace std;

#include <StatComputations.h>

namespace rmes {

  PhasedCounter::PhasedCounter(const short lmin, const short lmax,
			       const short nphases) :
    Counter(lmin,lmax),_nphases(nphases)
  {
    _phasedCounts.resize(nphases+1);
    _phasedMincounts.resize(nphases+1);
    _phasedMaxcounts.resize(nphases+1);
    for (short phase=0; phase<=nphases; phase++) {
      _phasedCounts[phase].resize(_counts.size());
      _phasedMincounts[phase].resize(_mincounts.size());
      _phasedMaxcounts[phase].resize(_maxcounts.size());
      for (short l=0;l<_counts.size();l++) {
	_phasedCounts[phase][l].resize(_counts[l].size());
	_phasedMincounts[phase][l]=_mincounts[l];
	_phasedMaxcounts[phase][l]=_maxcounts[l];
      }
    }
  }

  void
  PhasedCounter::countWords(const String &s, short sphase)
  {
    if (sphase==-1)
      sphase=positiveMod(s.length()-1,_nphases);
    for (short l=_lmin;l<=_lmax;l++) {
      fill(_counts[l].begin(),_counts[l].end(),0);
      for (short phase=0;phase<=_nphases;phase++) 
	fill(_phasedCounts[phase][l].begin(),_phasedCounts[phase][l].end(),0);
      for (long start=0;start<=s.length()-l;start++) {
	long wordnumber=s.substring(start,start+l-1);
	
	if (wordnumber>=0) {
		_counts[l][wordnumber]++;
	  _phasedCounts[_nphases][l][wordnumber]++;
	  _phasedCounts[positiveMod(sphase-s.length()+start+l,_nphases)][l][wordnumber]++;
	}
      }
    }

  }

  long *
  PhasedCounter::wordCounts(const short l, const short p)
  {
    return &(_phasedCounts[p][l][0]);
  }

  long
  PhasedCounter::wordCount(const short l, const long w, const short p) const
  {
    return _phasedCounts[p][l][w];
  }


  long
  PhasedCounter::maxCount(const short l, const short p)
  {
    if (_phasedMaxcounts[p][l]<0)
      _phasedMaxcounts[p][l]=*(max_element(_phasedCounts[p][l].begin(),
					   _phasedCounts[p][l].end()));
    return _phasedMaxcounts[p][l];
					   
  }


  long
  PhasedCounter::minCount(const short l, const short p)
  {
    if (_phasedMincounts[p][l]<0)
      _phasedMincounts[p][l]=*(min_element(_phasedCounts[p][l].begin(),
					   _phasedCounts[p][l].end()));
    return _phasedMincounts[p][l];
					   
  }

  long
  PhasedCounter::numWords(const short l) const
  {
    return Counter::numWords(l);
  }

  PhasedCounter::~PhasedCounter()
  {
  }


};
