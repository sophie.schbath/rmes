/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <Sequence.h>

#include <fstream>
#include <iostream>
#include <string>
using namespace std;

namespace rmes {

  Sequence::Sequence() 
  {
  }

  Sequence::Sequence(const string &name, const vector<Base> &seq) :
    _name(name),_seq(seq)
  {
  }

  Sequence::Sequence(const Sequence &s) 
  {
    _name=s._name;
    _seq=s._seq;
  }


  Sequence & 
  Sequence::operator=(const Sequence &s) 
  {
    if (this != &s) {
      _name=s._name;
      _seq=s._seq;
    }
    return *this;
  }



  long 
  Sequence::length() const
  {
    return _seq.size();
  }

  long 
  Sequence::begin() const
  {
    return 0;
  }

  long 
  Sequence::end() const
  {
    return _seq.size()-1;
  }

  long 
  Sequence::substring(const long start, const long end) const
  {
    long word=0;
    long size=end-start+1;
    vector<Base>::const_iterator it=_seq.begin()+start;
    for (int i=0;i<size;i++,it++) {
      if (*it == Alphabet::alphabet->interrupt()) {
	word = -1;
	break;
      }
      word=word*Alphabet::alphabet->factor()+*it;
    }
    return word;
  }


  Base 
  Sequence::operator[](const long index) const
  {
    return _seq[index];
  }


  Base 
  Sequence::first() const
  {
    Base lastBase=Alphabet::alphabet->interrupt();
    vector<Base>::const_iterator it=_seq.begin();
    while (it != _seq.end() && *it == Alphabet::alphabet->interrupt()) {
      ++it;
    }
    if (it != _seq.end())
      lastBase=*it;
    return lastBase;
  }
  
  Base 
  Sequence::last() const
  {
    Base lastBase=Alphabet::alphabet->interrupt();
    vector<Base>::const_reverse_iterator rit=_seq.rbegin();
    while (rit != _seq.rend() && *rit == Alphabet::alphabet->interrupt()) {
      ++rit;
    }
    if (rit != _seq.rend())
      lastBase=*rit;
    return lastBase;
  }
  
  const string &
  Sequence::name() const {
    return _name;
  }

  Sequence::~Sequence()
  {
  }

};
