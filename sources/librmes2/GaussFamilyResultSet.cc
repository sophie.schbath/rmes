/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <GaussFamilyResultSet.h>

#include <algorithm>
#include <iomanip>
using namespace std;

namespace rmes {
	GaussFamilyResultSet::GaussFamilyResultSet(const float minThresh, const float maxThresh) :
			FamilyResultSet(minThresh,maxThresh,"Gauss")
	{
		vector<HeaderStruct>::iterator hit=find(_headers.begin(),_headers.end(),HeaderStruct("score",0));
		_headers.insert(hit,HeaderStruct("sigma2",10));
	}

	void
	GaussFamilyResultSet::resize(short minSize, short maxSize) {
		FamilyResultSet::resize(minSize,maxSize);
		_sigma2.resize(maxSize+1);
		
	}

	istream &
	GaussFamilyResultSet::readParameter(istream &is, const string &h)
	{
		if (h=="sigma2") {
			float sigma2=readFloatValue(is);
			_sigma2[_minSize].push_back(sigma2);	
		} else
			FamilyResultSet::readParameter(is,h);

		return is;
	}	

	
	ostream &
	GaussFamilyResultSet::writeParameter(ostream &os, const short s, const HeaderStruct &h, const long index) const
	{
		if (h._name == "sigma2") 
			os << FIELDSEPARATOR << setw(h._width) << right << _sigma2[_minSize][index];
		else
			FamilyResultSet::writeParameter(os,s,h,index);
		return os;
	}

	GaussFamilyResultSet::~GaussFamilyResultSet()
	{
	}
};
