/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_FORMATPARAMETERS_H
#define RMES_FORMATPARAMETERS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <string>

namespace rmes {

  /**
   * Value Type class for the command line arguments.
   *
   * This class is only designed to grant easy access to the command line
   * arguments once it has been parsed by a ParameterParser instance.
   **/

  class FormatParameters  {
    
  public:
    FormatParameters();
    
    virtual ~FormatParameters() {};
    
    /*
     * Smallest word length given on the command-line or -1 if none
     * was given.
     */
    inline short minLength() const
    {
      return _minLength;
    }
    
    /**
     * Longest word length given on the command-line or -1 if none was
     * given.
     */
    inline short maxLength() const
    {
      return _maxLength;
    }
    
    /**
     * Minimum value for the threshold. Only used in order 1 Markov
     * models.
     **/
    inline float minThreshold() const
    {
      return _minThresh;
    }

    /**
     * Maximum value for the threshold. Only used in order 1 Markov
     * models.
     **/
    inline float maxThreshold() const
    {
      return _maxThresh;
    }

  protected:
    
    short _minLength;
    short _maxLength;

    float _minThresh;
    float _maxThresh;

    friend class FormatParameterParser;
    
  private:
    FormatParameters(const FormatParameters &) {};
    FormatParameters &operator=(const FormatParameters &) { return *this; };
  };
  
}
#endif
