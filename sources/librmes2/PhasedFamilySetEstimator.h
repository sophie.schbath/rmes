/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_PHASEDFAMILYSETESTIMATOR_H
#define RMES_PHASEDFAMILYSETESTIMATOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <vector>


#include <FamilySet.h>
#include <PhasedWordEstimator.h>

typedef std::vector<long> veclong;
typedef std::vector<veclong> matlong;


namespace rmes {

	class PhasedFamilySetEstimator : public PhasedWordEstimator {
		public:
			virtual void estimate(const Sequence &);
			virtual ~PhasedFamilySetEstimator();
		protected:
			PhasedFamilySetEstimator(const std::string &, const FamilySet &,
					const short, const short);
			virtual std::ostream &writeBlockToStream(std::ostream &);
			virtual std::ostream &writeAlphaToStream(std::ostream &);

			std::string _methodName;
			FamilySet _familySet;

			std::vector<matlong> _counts;

			PhasedCounter *_counterPlusOne;
			
	};
};


#endif
