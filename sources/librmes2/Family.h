/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_FAMILY_H
#define RMES_FAMILY_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <string>
#include <vector>


namespace rmes {

  class Family {
  public:
    Family(const long, const short);
    virtual ~Family();

    inline std::string name() const;
    inline short wordLength() const;
    inline long operator[](const long) const;
    inline void addWordNumber(const long);
    inline long numberOfWords() const;

    friend std::istream &operator>>(std::istream &, Family &);
    friend std::ostream &operator<<(std::ostream &, const Family &);

    friend class FamilySet;

  private :
    Family();
    std::string _name;
    std::vector<long> _wordNumbers;
    short _wordLength;
  };

  inline std::string
  Family::name() const
  {
    return _name;
  }

  inline short
  Family::wordLength() const
  {
    return _wordLength;
  }

  inline long
  Family::operator[](const long index) const
  {
    return _wordNumbers[index];
  }


  inline void
  Family::addWordNumber(const long wordNumber) 
  {
    _wordNumbers.push_back(wordNumber);
  }

  inline long
  Family::numberOfWords() const
  {
    return _wordNumbers.size();
  }

};

#endif
