/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedEstimator.h>

#include <algorithm>
#include <iostream>

using namespace std;

#include <WordSequence.h>

namespace rmes {
  
  PhasedEstimator::PhasedEstimator(const string &algorithm, short nphases) : 
    Estimator(algorithm), _nphases(nphases), _curphase(0)
  {
  }

  void
  PhasedEstimator::estimate(const Sequence &s)
  {
    _name=s.name();
  }

  double *
  PhasedEstimator::expectations(const short l, const short p)
  {
    return (&(_expect[p][l][0]));
  }

  double
  PhasedEstimator::expectation(const short l, const long w,const short p)
  {
    return _expect[p][l][w];
  }

  double *
  PhasedEstimator::variances(const short l,const short p)
  {
    return (&(_var[p][l][0]));
  }

  double
  PhasedEstimator::variance(const short l, const long w, const short p)
  {
    return _var[p][l][w];
  }


  double *
  PhasedEstimator::statistics(const short l, const short p)
  {
    return (&(_stat[p][l][0]));
  }

  double
  PhasedEstimator::statistic(const short l, const long w, const short p)
  {
    return _stat[p][l][w];
  }


  short
  PhasedEstimator::nPhases() const
  {
    return _nphases;
  }


  void
  PhasedEstimator::setCurrentPhase(const short p)
  {
    _curphase=p;
  }

  ostream &
  PhasedEstimator::writeBlockToStream(ostream &os)
  {
    WordSequence ws(1);
    os << "# " << PACKAGE << "-" << VERSION << endl
       << "# Alphabet = ";
    copy(ws.begin(),ws.end(),ostream_iterator<Word>(os," "));
    os << endl;
    os   << _name << " # " << _algorithm << " " << _curphase+1 << endl;
        return os;
  }

  PhasedEstimator::~PhasedEstimator()
  {
  }

  ostream &
  operator<<(ostream &os, const PhasedEstimator &ce)
  {
    PhasedEstimator &e=const_cast<PhasedEstimator &>(ce);
    switch (e._outputMode) {
    case PhasedEstimator::BLOCK :
      e.writeBlockToStream(os);
      break;
    case PhasedEstimator::ALPHA :
      e.writeAlphaToStream(os);
      break;
    }
    return os;
  }
  
};
