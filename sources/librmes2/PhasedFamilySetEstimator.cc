/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedFamilySetEstimator.h>

#include <iomanip>
#include <ios>
#include <iostream>
using namespace std;

namespace rmes {
	PhasedFamilySetEstimator::PhasedFamilySetEstimator(const string &n, const FamilySet &f,
			const short m, const short nphases) :
		PhasedWordEstimator(f.wordLength(),f.wordLength(),m,n+"_familles",nphases),
		_methodName(n+"_familles"), _familySet(f)
	{
		_counts.resize(_nphases+1);
		for (short p=0;p<=_nphases;p++) {
			_counts[p].resize(f.wordLength()+1);
			_counts[p][f.wordLength()].resize(f.numberOfFamilies());
		}
		
		_counter=new PhasedCounter(f.wordLength(),f.wordLength(),nphases);
		_counterPlusOne=new PhasedCounter((m==-1)?(f.wordLength()-1):(m+1), (m==-1)?(f.wordLength()-1):(m+1),nphases);
	}

	void
	PhasedFamilySetEstimator::estimate(const Sequence &s)
	{
		PhasedWordEstimator::estimate(s);
		_counter->countWords(s);
		_counterPlusOne->countWords(s);
	}

	ostream &
	PhasedFamilySetEstimator::writeBlockToStream(ostream &os)
	{
		PhasedEstimator::writeBlockToStream(os);
		os.setf(ios::fixed,ios::floatfield);
		os << _familySet.wordLength() << " " << _familySet.wordLength() << endl
			<< _familySet.numberOfFamilies() << " " << _familySet.wordsPerFamily() << endl
			<< _familySet.fileName() << endl
			<< _familySet.wordLength() << endl
			<< markovOrder(_familySet.wordLength())
			<< " " << markovOrder(_familySet.wordLength()) << endl;

		for (long familyIndex=0; familyIndex < _familySet.numberOfFamilies();
					familyIndex++) {
			if (familyIndex % NCOLUMNS == 0)
				os << endl;
			os << _familySet[familyIndex].name() << " "
				<< _counts[_curphase][_familySet.wordLength()][familyIndex] << " "
				<< setprecision(3)
				<< _expect[_curphase][_familySet.wordLength()][familyIndex] << " "
				<< setprecision(3)
				<< _var[_curphase][_familySet.wordLength()][familyIndex] << " "
				<< setprecision(4)
				<< _stat[_curphase][_familySet.wordLength()][familyIndex] << " " << endl;
		}
		
		return os;
	}

	ostream &
	PhasedFamilySetEstimator::writeAlphaToStream(ostream &os) {
		return os;
	}

	PhasedFamilySetEstimator::~PhasedFamilySetEstimator()
	{
		delete _counter;
		delete _counterPlusOne;
	}
};
