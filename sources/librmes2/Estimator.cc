/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <Estimator.h>

#include <algorithm>
#include <iostream>

using namespace std;

#include <WordSequence.h>

namespace rmes {
  
  Estimator::Estimator(const string &algorithm) : 
    _algorithm(algorithm), _outputMode(BLOCK)
  {
  }

  void
  Estimator::estimate(const Sequence &s)
  {
    _name=s.name();
  }

  ostream &
  Estimator::writeBlockToStream(ostream &os)
  {
    WordSequence ws(1);
    os << "# " << PACKAGE << "-" << VERSION << endl
       << "# Alphabet = ";
    copy(ws.begin(),ws.end(),ostream_iterator<Word>(os," "));
    os << endl;
    return os;
  }


  ostream &
  Estimator::writeAlphaToStream(ostream &os)
  {
    return os;
  }

  Estimator::~Estimator()
  {
  }

  ostream &
  operator<<(ostream &os, const Estimator &ce)
  {
    Estimator &e=const_cast<Estimator &>(ce);
    switch (e._outputMode) {
    case Estimator::BLOCK :
      e.writeBlockToStream(os);
      break;
    case Estimator::ALPHA :
      e.writeAlphaToStream(os);
      break;
    }
    return os;
  }
  
};
