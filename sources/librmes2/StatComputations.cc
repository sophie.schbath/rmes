/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <StatComputations.h>

#include <cfloat>
#include <cmath>
#include <iostream>
#include <limits>
using namespace std;

extern "C" {

#include <cdflib.h>

}

namespace rmes {


  
  bool 
  equal(const double d1, const double d2)
  {
    return (fabs(d1-d2) <= DBL_EPSILON); 
  }

  double
  computeStat(const long count, const double expect, const double var)
  {
    double stat=0;
    if (var <= 0.0) { 
      if (!equal(double(count),expect)) {
	if (double(count)>expect)
	  stat=9999.0;
	else
	  stat=-9999.0;
      }
    } else {
      if (!equal(double(count),expect))
	stat=(double(count)-expect)/sqrt(var);
    }
    return stat;
  }

  double
  quantile(const double p)
  {
    static const double c0=2.515515;
    static const double c1=0.802853;
    static const double c2=0.010328;
    static const double d1=1.432788;
    static const double d2=0.189269;
    static const double d3=0.001308;

    static short sign=0;

    static double tmp, tmp1, tmp2;

    double ret=0.0;



    if (p < 0.0 || p > 1.0 || p == numeric_limits<double>::infinity()) {
      ret=numeric_limits<double>::quiet_NaN();
    } else {
      if (p <= 0.5) {
	sign=+1;
	tmp= -2.0 * log(p);
      } else {
	sign=-1;
	tmp= -2.0 * log(1.0 - p);
      }
      if (tmp == numeric_limits<double>::infinity()) {
	ret=tmp;
      } else {
	tmp = sqrt(tmp);
	tmp1 = c0 + (c1 + c2*tmp) * tmp;
	tmp2 = 1.0 + (d1 + (d2 + d3 * tmp) * tmp) * tmp;
	ret = tmp - tmp1 / tmp2;
      }
      ret = sign*ret;
    }

    return ret;
  }


  double 
  gamma(const double x)
  {
    static const double cof[]={
       76.18009173,
      -86.50532033,
       24.01409822,
       -1.231739516,
        0.120858003e-2,
       -0.53682e-5
    };

    double y = x - 1.0;
    double tmp = y + 5.5;
    tmp -= (y + 0.5) * log(tmp);
    double ser = 1.0;
    for (short n=0;n<=5;n++) {
      y += 1.0;
      ser += cof[n]/y;
    }
    return (-tmp + log (2.50662827465 * ser));
    
  }

  const double PRECISION=5.0e-16;
  double
  computeInfPoisson(const double a, const double x)
  {
    static const int MAXITER=200;
    double retval=-1.0;

    bool ok=false;

    if (a<=0.0 || x<=0.0) {
      cerr << "Error in computeInfPoisson: one of the arguments is lesser than or equal to zero." << endl;
    } else {
      double a0=0.0;
      double b0=1.0;
      double a1=1.0;
      double b1=x;
      
      double factor=1.0;
      
      double sumOne;
      double sumTwo=a1/b1;
      
      for (short n=1; n<= MAXITER; n++) {
	a0=(a1+ (n-a) * a0) * factor;
	b0=(b1+ (n-a) * b0) * factor;
	a1= x * a0 + n * a1 * factor;
	b1= x * b0 + n * b1 * factor;
	if (b1 != 0.0) {
	  sumOne = a1/b1;
	  if (fabs((sumOne-sumTwo)/sumOne) < PRECISION) {
	    retval=sumOne*exp(-x + a * log(x) - gamma(a));
	    ok=true;
	    break;
	  }
	  sumTwo=sumOne;
	  factor=1.0/b1;
	}
      }
      if (!ok) {
	cerr << "Error in computeInfPoisson: not enough iterations." << endl;
	retval=-1.0;
      }
    }

    return retval;
  }

  double
  computeSupPoisson(const double a, const double x)
  {
    static const int MAXITER = 500;

    double retval = -1.0;
    bool ok=false;

    if (x <= 0.0 || a <= 0.0) {
      cerr << "Error in computeSupPoisson: one of the arguments is lesser than or equal to zero." << endl;
    } else {
      double term = 1.0 / a;
      double sum = term;
      for (short n=1; n<=MAXITER; n++) {
	term *= x / (a+n);
	sum  += term;
	if (fabs(term/sum) < PRECISION) {
	  retval = sum * exp(-x + a * log(x) - gamma(a));
	  ok=true;
	  break;
	}
      }
      if (!ok) {
	cerr << "Error in computeSupPoisson: not enough iterations." << endl;
	retval=-1.0;
      }
    }
    return retval;
  }

  double
  computeStatPoisson(const long n, const double x)
  {
    static const double c0=2.515517;
    static const double c1=0.802853;
    static const double c2=0.010328;

    static const double d1=1.432788;
    static const double d2=0.189269;
    static const double d3=0.001308;

    double retval=numeric_limits<double>::quiet_NaN();

    if (x<0.0 || n <0) {
      cerr << "Error in computeStatPoisson: one of the arguments is lesser than or equal to zero." << endl; 
    } else {
      if (x == 0.0) {
	retval=numeric_limits<double>::infinity();
      } else {
	double a= n+1;
	double proba=-1;
	short sign=0;
	if (x < (a+1)) {
	  proba=computeSupPoisson(a,x);
	  if (proba == -1.0)
	    retval=numeric_limits<double>::quiet_NaN();
	  if (proba <= 0.5) 
	    sign = +1;
	  else {
	    sign = -1;
	    proba = 1.0 - proba;
	  }
	} else {
	  proba=computeInfPoisson(a,x);
	  if (proba == -1.0)
	    retval=numeric_limits<double>::quiet_NaN();
	  if (proba <= 0.5)
	    sign = -1;
	  else {
	    sign = +1;
	    proba = 1.0 - proba;
	  }
	}
	double tmp= -2.0 * log(proba);
	if (tmp==numeric_limits<double>::infinity()) 
	  retval = tmp;
	else {
	  tmp = sqrt(tmp);
	  double tmp1 = c0 + (c1 + c2 * tmp) * tmp;
	  double tmp2 = 1.0 + (d1 + (d2 + d3 * tmp) * tmp) *tmp;
	  retval = tmp - tmp1/tmp2;
	}
	retval = sign * retval;
      }
    }
    return retval;
  }

  short positiveMod(const long v, const short p)
  {
    short mod=v%p;

    return ((mod>=0)?(mod):(mod+p));
  }



/* ----------------------------------------------------------- */
/* ...Fonction qui calcule la queue de distribution a gauche...*/
/* ...d'une loi de Polya-Aeppli de parametres (lambda, a) : ...*/
/* ...P(PA<=n) avec n un entier. D'apres Nuel (2006)        ...*/
/* ----------------------------------------------------------- */

  double lower_tail(const double lambda, const double a, const long n) {
    /* Variables internes : .......................................... */
    double z ; /* ((1-a) * lambda / a) */
    double L_prec, L_cour, L_suiv, A_cour, A_suiv, S_cour, S_suiv ;
    long i ;
    /* Constantes internes : .......................................... */
    static const double infinity=1e300;

    /* Algorithme : .......................................... */
    z = (1.0-a) * lambda / a ; 
    if (n==0)
      return(exp(-1.0*lambda)) ;
    else if (n==1)
      return(exp(-1.0*lambda)*(1.0+a*z));
    else
      {
	L_prec = -lambda ;
	L_cour = L_prec + log(a*z) ;
	A_cour = L_prec ;
	S_cour = 1.0 + a*z ;
	for (i = 2 ; i<=n ; i++)
	  {
	    L_suiv = L_cour + log( a*(2.0*i-2.0+z)/i + a*a*(2.0-i)*exp(L_prec-L_cour)/i ) ;
	    S_suiv = S_cour + exp(L_suiv-A_cour) ;
	    if ( (S_suiv>0) && (S_suiv<infinity))
	      A_suiv = A_cour ;
	    else
	      {
		A_suiv = A_cour + log(S_cour) ;
		S_suiv = 1.0 + exp(L_suiv-A_suiv) ;
	      }
	    L_prec = L_cour ;
	    L_cour = L_suiv ;
	    S_cour = S_suiv ;
	    A_cour = A_suiv ;
	  } 
	return(S_cour * exp(A_cour)); 
      }
  }    


  /* ----------------------------------------------------------- */
  /* ...Fonction qui calcule la queue de distribution a droite...*/
  /* ...d'une loi de Polya-Aeppli de parametres (lambda, a) : ...*/
  /* ...P(PA>=n) avec n un entier. D'apres Nuel (2006)        ...*/
  /* ----------------------------------------------------------- */

  double upper_tail(const double lambda, const double a, const long n) {
    /* ........ Variables internes : .......................................... */
    double z ; /* (theta * lambda / a) */
    double L_prec, L_cour, L_suiv, A_cour, A_suiv, S_cour, S_suiv ;
    long i ;
    bool converged=false ;
    /* Constantes internes : .......................................... */
    static const double precision=1e-13;
    static const double infinity=1e300;
    
    /* ........ Algorithme : .......................................... */
    if (n==0)
      return(1.0) ;
    else if (n==1)
      return(1.0 - exp(-lambda));
    else
      {
	z = (1.0-a) * lambda / a ; 
	/* initialisations */    
	L_prec = -lambda ;  /* ln P(PA=0) */
	L_cour = L_prec + log(a*z) ;/* ln P(PA=1) */
	/* recurrence pour calculer ln P(PA=i) */
	for (i = 2 ; i<=n ; i++)  
	  {
	    L_suiv = L_cour + log( a*(2.0*i-2.0+z)/i + a*a*(2.0-i)*exp(L_prec-L_cour)/i ) ;
	    L_prec = L_cour ;
	    L_cour = L_suiv ;     
	  }
	A_cour = L_suiv ;  /* ln P(PA=n) */
	S_cour = 1.0 ;
	/* i=n+1 */
	while (!converged)
	  {
	    L_suiv = L_cour + log( a*(2.0*i-2.0+z)/i + a*a*(2.0-i)*exp(L_prec-L_cour)/i ) ;
	    
	    if (L_suiv < log(precision) + A_cour + log(S_cour))
	      converged = true ;
	    
	    S_suiv = S_cour + exp(L_suiv-A_cour) ;
	    if ( (S_suiv>0) && (S_suiv<infinity))
	      A_suiv = A_cour ;
	    else
	      {
		A_suiv = A_cour + log(S_cour) ;
		S_suiv = 1.0 + exp(L_suiv-A_suiv) ;
	      } 
	    L_prec = L_cour ;
	    L_cour = L_suiv ;     
	    S_cour = S_suiv ;
	    A_cour = A_suiv ;  
	    i=i+1; 
	  }
	
	return(S_cour * exp(A_cour));
      }
  }
  

  void
  binomial_tails(const long count, const long thresh, const double pi0,double *p, double *q)
  {
    int which=1;
    *p=0.0;
    *q=0.0;
    double s=(double)thresh;
    double xn=(double)count;
    double pr=pi0;
    double ompr=(double)1.0-pi0;
    int status=0;
    double bound=0;

    cdfbin(&which,p,q,&s,&xn,&pr,&ompr,&status,&bound);
    if (status != 0) {
      cerr << "cdbfin return status: " << status << "(bound = " << bound << ")" << endl;
    }
    //cerr << "count: " << count << "\tthresh: " << thresh << "\tpr: " << pr << "\tompr: " << ompr << "\tp: " << p << "\tq: " << q << endl;
  }

  double
  binomial_lower_tail(const long count, const long thresh, const double pi0)
  {
    int which=1;
    double p=0.0;
    double q=0.0;
    double s=(double)thresh;
    double xn=(double)count;
    double pr=pi0;
    double ompr=(double)1.0-pi0;
    int status=0;
    double bound=0;
    cdfbin(&which,&p,&q,&s,&xn,&pr,&ompr,&status,&bound);
    if (status != 0) {
      cerr << "cdbfin return status: " << status << "(bound = " << bound << ")" << endl;
    }
    //cerr << "count: " << count << "\tthresh: " << thresh << "\tpr: " << pr << "\tompr: " << ompr << "\tp: " << p << "\tq: " << q << endl;
    return p;
  }

  double
  binomial_upper_tail(const long count, const long thresh, const double pi0)
  {
  int which=1;
    double p=0.0;
    double q=0.0;
    double s=(double)thresh-1;
    double xn=(double)count;
    double pr=pi0;
    double ompr=(double)1.0-pi0;
    int status=0;
    double bound=0;
    cdfbin(&which,&p,&q,&s,&xn,&pr,&ompr,&status,&bound);
    if (status != 0) {
      cerr << "cdbfin return status: " << status << "(bound = " << bound << ")" << endl;
    }
    //cerr << "count: " << count << "\tthresh: " << thresh << "\tpr: " << pr << "\tompr: " << ompr << "\tp: " << p << "\tq: " << q << endl;
    return q;
  }


};
