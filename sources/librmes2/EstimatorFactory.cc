/**
   Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

   This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

   RMES is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   RMES is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with RMES; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <EstimatorFactory.h>

#include <sstream>
using namespace std;

#include <CompoundPoissonFamilySetEstimator.h>
#include <CompoundPoissonWordEstimator.h>
#include <FamilySet.h>
#include <FamilySetFactory.h>
#include <GaussFamilySetEstimator.h>
#include <GaussWordEstimator.h>
#include <MartFamilySetEstimator.h>
#include <MartWordEstimator.h>
#include <PhasedGaussFamilySetEstimator.h>
#include <PhasedGaussWordEstimator.h>
#include <PhasedMartFamilySetEstimator.h>
#include <PhasedMartWordEstimator.h>
#include <PoissonWordEstimator.h>
#include <RMESException.h>
#include <SkewFamilySetEstimator.h>
#include <SkewWordEstimator.h>

namespace rmes {

  
  Estimator *
  EstimatorFactory::getEstimator(const RMESParameters &p)
  {
    Estimator *e=NULL;

    string exceptstring;
    ostringstream exceptstream(exceptstring);

    string wordtype="all words";

    short markovOrder=p.markovOrder();
    if (markovOrder<0)
      markovOrder=1;

    if (p.familyFile().length()) {//On traite le cas "famille de mots"
      
      wordtype="word families";
      FamilySetFactory fsFact(p.familyFile());
      fsFact.hasMoreFamilySets();
      FamilySet fs=fsFact.nextFamilySet();
      
      
      if (p.maxOrder()==true)
	markovOrder=-1;
      else {
	short wordLength=fs.wordLength();
	if (markovOrder > wordLength-2) {
	  exceptstream << "wrong Markov order (" << markovOrder << ") for method " 
		       << p.method() << " and families of words with length " << wordLength;
	  throw NoSuchEstimatorException(exceptstream.str());
	}
      }
	
      if (p.method() == "Gauss")
	if (p.phases() == 0)
	  if (p.maxOrder() == true || (markovOrder==fs.wordLength()-2))
	    e=new MartFamilySetEstimator(fs);
	  else
	    e=new GaussFamilySetEstimator(fs,markovOrder);
	else
	  if (p.maxOrder() == true || (markovOrder==fs.wordLength()-2))
	    e=new PhasedMartFamilySetEstimator(fs,p.phases());
	  else
	    e=new PhasedGaussFamilySetEstimator(fs,markovOrder,p.phases());
	
      if (p.method() == "compound Poisson")
	e=new CompoundPoissonFamilySetEstimator(fs,markovOrder);
	

      if (p.method()=="skew") 
	e=new SkewFamilySetEstimator(fs,markovOrder);
      
    } else {//On traite le cas "mots" 
      
      short minLength=p.minLength();
      short maxLength=p.maxLength();
      if (p.maxOrder()==true) 
	markovOrder=-1;
      else {
	if (markovOrder>minLength-2) {
	  exceptstream << "wrong Markov order (" << markovOrder << ") for method " 
		       << p.method() << " and minimal word length " << minLength;
	  throw NoSuchEstimatorException(exceptstream.str());
	}
      }
      
      if (p.method() == "skew") {
	e=new SkewWordEstimator(minLength,maxLength,markovOrder);
      }

      if (p.method() == "Gauss") {
	if (p.phases() == 0)

	  if (p.maxOrder() == true || 
	      (minLength == maxLength && markovOrder == minLength -2))
	    e=new MartWordEstimator(minLength,maxLength);
	  else
	    e=new GaussWordEstimator(minLength,maxLength,markovOrder);
	else
	  if (p.maxOrder() == true ||
	      (minLength == maxLength && markovOrder == minLength -2))
	    e=new PhasedMartWordEstimator(minLength,maxLength,p.phases());
	  else
	    e=new PhasedGaussWordEstimator(minLength,maxLength,markovOrder,
					   p.phases());
      }
	
      if (p.method() == "compound Poisson")
	e=new CompoundPoissonWordEstimator(minLength,maxLength,markovOrder);
	
      if (p.method() == "Poisson") 
	e=new PoissonWordEstimator(minLength,maxLength,markovOrder);

    }
      
    if (!e)
      throw NoSuchEstimatorException("there is no estimator applicable to "+
				     wordtype+" for method "+p.method()+".");
    return e;
  }
};
