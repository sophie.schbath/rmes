/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedWordEstimator.h>

#include <algorithm>
#include <cmath>
#include <memory>
using namespace std;

#include <StatComputations.h>
#include <WordSequence.h>

namespace rmes {

  PhasedWordEstimator::PhasedWordEstimator(const short lmin, const short lmax,
					   const short m, 
					   const string &algorithm,
					   const short nphases) : 
    PhasedEstimator(algorithm,nphases),
    MarkovEstimator(m),
    _lmin(lmin), _lmax(lmax)
  {
    _expect.resize(nphases+1);
    _var.resize(nphases+1);
    _stat.resize(nphases+1);
    for (short p=0;p<=nphases;p++) {
      _expect[p].resize(lmax+1);
      _var[p].resize(lmax+1);
      _stat[p].resize(lmax+1);
      for (short l=lmin; l<=lmax; l++) {
	_expect[p][l].resize(1<<(Alphabet::alphabet->bits()*l));
	_var[p][l].resize(1<<(Alphabet::alphabet->bits()*l));
	_stat[p][l].resize(1<<(Alphabet::alphabet->bits()*l));
      }
    }
  }
  

  void
  PhasedWordEstimator::estimate(const Sequence &s)
  {
    _name=s.name();

    for (short p=0;p<=_nphases;p++)
      for (short l=_lmin; l<=_lmax; l++) {
	fill(_expect[p][l].begin(),_expect[p][l].end(),0.0);
	fill(_var[p][l].begin(),_var[p][l].end(),0.0);
	fill(_stat[p][l].begin(),_stat[p][l].end(),0.0);
      }
  }

  void
  PhasedWordEstimator::countWords(const Sequence &s)
  {
    _counter->countWords(s);
  }


  short
  PhasedWordEstimator::minLength() const
  {
    return _lmin;
  }

  short
  PhasedWordEstimator::maxLength() const
  {
    return _lmax;
  }

  long *
  PhasedWordEstimator::wordCounts(const short l, const short p)
  {
    return _counter->wordCounts(l,p);
  }

  long
  PhasedWordEstimator::wordCount(const short l, const long w, const short p)
  {
    return _counter->wordCount(l,w,p);
  }

  long
  PhasedWordEstimator::numWords(const short l)
  {
    return _counter->numWords(l);
  }

  void
  PhasedWordEstimator::setOutputMode(OutputMode om)
  {
    _outputMode=om;
  }
  double
  PhasedWordEstimator::condAsExpect(const PhasedWord &w,
					 const short m,
					 const PhasedCounter &c)
  {
    short mplusone=m+1;
    long firstword=w.substring(0,m);
    short firstphase=positiveMod((w.phase()-w.length()+mplusone),_nphases);
    double expect=double(c.wordCount(mplusone,firstword,firstphase));

    for (int l=1; l<w.length()-m; l++) {
      long curSubWord=w.substring(l,l+m-1);
      long mphase=positiveMod((firstphase+l),_nphases);
      double quantity=0.0;
      for (int letter=0; letter<Alphabet::alphabet->size();letter++)
	quantity+=double(c.wordCount(mplusone,Alphabet::alphabet->factor()*curSubWord+letter,mphase));
      if (quantity == 0.0) {
	expect=0.0;
	break;
      } else {
	long neighbor=curSubWord*Alphabet::alphabet->factor()+w[l+m];
	expect*=double(c.wordCount(mplusone,neighbor,mphase))/quantity;
      }
    }

    return expect;
  }

  double 
  PhasedWordEstimator::condAsVar(const PhasedWord &w, const short m,
				      const PhasedCounter &seqCounter,
				      const PhasedCounter &wordCounter,
				      const double expect)
  {
    short mplusone=m+1;
    double var=0.0;
    if (expect != 0.0) {
      for (long word=0;word<seqCounter.numWords(mplusone);
	   word+=Alphabet::alphabet->factor()) {
	for (short phase=0; phase<_nphases;phase++) {
	  double sum1=0.0;
	  for (int letter=0; letter<Alphabet::alphabet->size();letter++)
	    sum1+=double(seqCounter.wordCount(mplusone,word+letter,phase));
	  if (sum1>0.0) {
	    double sum2=0.0;
	    for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	      sum2+=double(wordCounter.wordCount(mplusone,word+letter,phase));
	    }
	    var += sum2*sum2/sum1;
	    for (long wordplusone=word;wordplusone<word+Alphabet::alphabet->size();wordplusone++) {
	      double wordplusoneSeqCount=seqCounter.wordCount(mplusone,wordplusone,phase);
	      if (wordplusoneSeqCount >0 ) {
		double wordplusoneWordCount=wordCounter.wordCount(mplusone,wordplusone,phase);
		var -= wordplusoneWordCount * wordplusoneWordCount / wordplusoneSeqCount;
	      }
	    }
	  }
	}
      }
      long subWord=Alphabet::alphabet->factor()*w.substring(0,m-1);
      short subWordPhase=positiveMod((w.phase()-w.length()+mplusone),_nphases);
      double wordSum=0.0;
      double seqSum=0.0;
      for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	wordSum+=wordCounter.wordCount(mplusone,subWord+letter,subWordPhase);
	seqSum+=seqCounter.wordCount(mplusone,subWord+letter,subWordPhase);
      }
      var += (1.0-2.0*wordSum)/seqSum;
      var *= expect*expect;
      var += expect;
      for (short d=1; d < w.length()-m; d++) {
	auto_ptr<PhasedWord> ow(w.overlaps(w,d));
	if (ow.get()) {
	  var += 2.0 * condAsExpect(*(ow.get()),m,seqCounter);
	}
      }
    }
    return var;
  }

  double 
  PhasedWordEstimator::condAsCoVar(const PhasedWord &w1, const PhasedWord &w2, const short m,
				   const PhasedCounter &seqCounter,  const PhasedCounter &wordCounter,
				   const double expect1, const double expect2)
  {
    double covar=0.0;
    if (w1==w2) {
      covar=condAsVar(w1,m,seqCounter,wordCounter,expect1);
    } else {
      short mplusone=m+1;
      if (expect1 == 0.0 || expect2 == 0.0)
	covar=0.0;
      else {
	PhasedCounter word2Counter(mplusone,mplusone,_nphases);
	word2Counter.countWords(w2,w2.phase());
	for (long word=0;word<seqCounter.numWords(mplusone);
	     word+=Alphabet::alphabet->factor()) {
	  for (short phase=0;phase<_nphases;phase++) {
	    double sum1=0.0;
	    for (int letter=0;letter<Alphabet::alphabet->size();letter++)
	      sum1+=seqCounter.wordCount(mplusone,word+letter,phase);
	    if (sum1>0.0) {
	      double w1Count=0;
	      double w2Count=0;
	      for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
		w1Count+=wordCounter.wordCount(mplusone,word+letter,phase);
	      }
	      for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
		w2Count+=word2Counter.wordCount(mplusone,word+letter,phase);
	      }
	      covar += w1Count*w2Count/sum1;
	      for (short letter=0; letter < Alphabet::alphabet->size(); letter++) {
		      long wordm=word+letter;
		      if (seqCounter.wordCount(mplusone,wordm,phase) > 0) {
			      covar -= (double)wordCounter.wordCount(mplusone,wordm,phase) *
				      (double)word2Counter.wordCount(mplusone,wordm,phase) /
				      (double)seqCounter.wordCount(mplusone,wordm,phase);
		      }
	      }
	    }
	  }
	}
	long subwordW1=Alphabet::alphabet->factor()*w1.substring(0,m-1);
	long subwordW2=Alphabet::alphabet->factor()*w2.substring(0,m-1);
	short subphase=positiveMod((w1.phase()-w1.length() + mplusone),
				   _nphases);
	if ((subwordW1 == subwordW2) && (w1.phase() == w2.phase())) {
	  double w1Count=0.0;
	  double w2Count=0.0;
	  double seqCount=0.0;
	  for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	    w1Count+=wordCounter.wordCount(mplusone,subwordW1+letter,subphase);
	  }
	  for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	    w2Count+=word2Counter.wordCount(mplusone,subwordW1+letter,
					    subphase);
	  }
	  for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	    seqCount+=seqCounter.wordCount(mplusone,subwordW1+letter,subphase);
	  }
	  covar += (1.0 - w1Count - w2Count) / seqCount;
	} else {
	  short subphaseW2=positiveMod((w2.phase()-w1.length() + mplusone),
				       _nphases);
	  double sum1=0.0;
	  double sum2=0.0;
	  double sum3=0.0;
	  double sum4=0.0;
	  for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	    sum1+=wordCounter.wordCount(mplusone,subwordW2+letter,subphaseW2);
	  }
	  for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	    sum2+=seqCounter.wordCount(mplusone,subwordW2+letter,subphaseW2);
	  }
	  for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	    sum3+=word2Counter.wordCount(mplusone,subwordW1+letter,subphase);
	  }
	  for (int letter=0; letter<Alphabet::alphabet->size();letter++) {
	    sum4+=seqCounter.wordCount(mplusone,subwordW1+letter,subphase);
	  }
	  covar -= sum1/sum2;
	  covar -= sum3/sum4;
	}
	covar *= expect1 * expect2;
	for (short d=mplusone-w1.length(); d<w1.length()-m;d++) {
	  auto_ptr<PhasedWord> ow(w1.overlaps(w2,d));
	  if (ow.get()) 
	    covar += condAsExpect(*(ow.get()),m,seqCounter);
	}
      }
    }
    return covar;
  }
  
  ostream &
  PhasedWordEstimator::writeBlockToStream(ostream &os)
  {
    PhasedEstimator::writeBlockToStream(os);
    static const int DEFAULTPRECISION=6;
    os.setf(ios::fixed,ios::floatfield);
    os  << _lmin << " " << _lmax << endl;
    for (short l=_lmin;l<=_lmax;l++) {
      os << endl 
	 << markovOrder(l) << " " << markovOrder(l) << endl;
      WordSequence ws(l);
      long columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _counter->wordCount(l,it->number(),_curphase);
      }
      os << endl;
      os.precision(3);
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _expect[_curphase][l][it->number()];
      }
      os << endl;
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _var[_curphase][l][it->number()];
      }
      os << endl;

      os.precision(4);
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _stat[_curphase][l][it->number()];
      }
      os << endl ;
      os.precision(DEFAULTPRECISION);
    }
    return os;
  }


  PhasedWordEstimator::~PhasedWordEstimator()
  {
  }
};



