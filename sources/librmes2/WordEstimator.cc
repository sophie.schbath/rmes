/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <WordEstimator.h>

#include <algorithm>
#include <cmath>
#include <memory>
#include <set>
using namespace std;

#include <WordSequence.h>

namespace rmes {

  WordEstimator::WordEstimator(const short lmin, const short lmax,
			       const short m, const string &algorithm) : 
    SimpleEstimator(algorithm),
    MarkovEstimator(m),
    _lmin(lmin), _lmax(lmax)
  {
    _expect.resize(lmax+1);
    _var.resize(lmax+1);
    _stat.resize(lmax+1);
    for (short l=lmin; l<=lmax; l++) {
      _expect[l].resize(1<<(Alphabet::alphabet->bits()*l));
      _var[l].resize(1<<(Alphabet::alphabet->bits()*l));
      _stat[l].resize(1<<(Alphabet::alphabet->bits()*l));
    }
  }
  

  void
  WordEstimator::estimate(const Sequence &s)
  {
    _name=s.name();

    for (short l=_lmin; l<=_lmax; l++) {
      fill(_expect[l].begin(),_expect[l].end(),0.0);
      fill(_var[l].begin(),_var[l].end(),0.0);
      fill(_stat[l].begin(),_stat[l].end(),0.0);
    }
    
  }

  void
  WordEstimator::countWords(const Sequence &s)
  {
    _counter->countWords(s);
  }


  short
  WordEstimator::minLength() const
  {
    return _lmin;
  }

  short
  WordEstimator::maxLength() const
  {
    return _lmax;
  }

  long *
  WordEstimator::wordCounts(const short l)
  {
    return _counter->wordCounts(l);
  }

  long
  WordEstimator::wordCount(const short l, const long w)
  {
    return _counter->wordCount(l,w);
  }

  long
  WordEstimator::numWords(const short l)
  {
    return _counter->numWords(l);
  }

  void
  WordEstimator::setOutputMode(OutputMode om)
  {
    _outputMode=om;
  }


  double
  WordEstimator::condAsExpect(const Word &w, const short m, const Counter &c)
  {

    long leftSubWord=w.substring(0,m);
    double expect=double(c.wordCount(m+1,leftSubWord));
    for (int l=1; l<w.length()-m;l++) {
      long curSubWord=w.substring(l,l+m-1);
      double quantity=0.0;
      for (int letter=0;letter<Alphabet::alphabet->size();letter++)
	quantity+=double(c.wordCount(m+1,Alphabet::alphabet->factor()*curSubWord+letter));
      if (quantity == 0.0) {
	expect=0.0;
	break;
      } else {
	long neighbor=curSubWord*Alphabet::alphabet->factor()+w[l+m];
	expect*= double(c.wordCount(m+1,neighbor))/quantity;
      }
    }
    return expect;
  }


  double
  WordEstimator::condAsVar(const Word &w, const short m, const Counter &cc, const double expect)
  {
    Counter &c=const_cast<Counter &>(cc);
    double var=0.0;
    long factor=Alphabet::alphabet->factor();
    long size=Alphabet::alphabet->size();
    //    double expect=condAsExpect(w,m,c);
    if (expect != 0.0) {
      Counter subWordCounter(m+1,m+1);
      subWordCounter.countWords(w);
      set<long> subwords;
      for (int start=0;start<w.length()-m+1;start++) {
	subwords.insert(w.substring(start,start+m-1));
      }
      for (set<long>::iterator wit=subwords.begin();wit!=subwords.end();wit++) {
	long wordIndex=*wit;
	//      for (long wordIndex=0; 
	//	   wordIndex < c.numWords(m+1)/factor;
	//	   wordIndex++) {
	double q1=0.0;
	for (long baseIndex=0;baseIndex<size;baseIndex++)
	  q1+=double(c.wordCount(m+1,factor*wordIndex+baseIndex));
	if (q1 > 0.0) {
	  double q2=0;
	  for (long baseIndex=0;baseIndex<size; baseIndex++)
	    q2+=double(subWordCounter.wordCount(m+1,
						factor*wordIndex+
						baseIndex));
	  var += (q2*q2)/q1;
	  for (long baseIndex=0; baseIndex<size; baseIndex++) {
	    long neighborIndex=factor*wordIndex+baseIndex;
	    if (c.wordCount(m+1,neighborIndex)>0) {
	      var -= double(subWordCounter.wordCount(m+1,neighborIndex)*
		     subWordCounter.wordCount(m+1,neighborIndex))/
		     double(c.wordCount(m+1,neighborIndex));
	    }
	  }
	}
      }
      long firstIndex=w.substring(0,m-1);
      long seqCounts=0;
      long wordCounts=0;
      for (int baseIndex=0;baseIndex<size;baseIndex++) {
	seqCounts += c.wordCount(m+1,factor*firstIndex+baseIndex);
	wordCounts += 
	  subWordCounter.wordCount(m+1,
				   factor*firstIndex+baseIndex);

      }
      var += (1.0 - 2.0 * double(wordCounts)) / double (seqCounts);
      var *= expect * expect;
      var += expect;

      for (long d=1; d < w.length()-m; d++) {
	auto_ptr<Word> overlapWord(w.overlaps(d));
	if (overlapWord.get()) {
	  var += 2.0 * condAsExpect(*overlapWord.get(),m,c);
	}
	
      }
    }
    return var;
  }


  double
  WordEstimator::condAsCoVar(const Word &w1, const Word &w2, 
			     const short m, const Counter &cc)
  {
    Counter &c=const_cast<Counter &>(cc);
    double covar=0.0;
    if (w1==w2) {
      double expect=condAsExpect(w1,m,cc);
      covar=condAsVar(w1,m,cc,expect);
    } else {
      double expectW1=condAsExpect(w1,m,cc);
      double expectW2=condAsExpect(w2,m,cc);
      if (expectW1 != 0.0 && expectW2 != 0.0) {
	Counter wordCounterW1(m+1,m+1);
	wordCounterW1.countWords(w1);
	Counter wordCounterW2(m+1,m+1);
	wordCounterW2.countWords(w2);
	for (long wordIndex=0; wordIndex < c.numWords(m+1)/Alphabet::alphabet->factor();
	     wordIndex++) {
	  double q1=0.0;
	  for (long baseIndex=0; baseIndex < Alphabet::alphabet->size(); baseIndex++)
	    q1+=double(c.wordCount(m+1,wordIndex*Alphabet::alphabet->factor()+baseIndex));
	  if (q1 > 0.0) {
	    double countW1=0.0;
	    double countW2=0.0;
	    for (long baseIndex=0; baseIndex < Alphabet::alphabet->size(); baseIndex++) {
	      countW1+=double(wordCounterW1.wordCount(m+1,
				  wordIndex*Alphabet::alphabet->factor()+baseIndex));
	      countW2+=double(wordCounterW2.wordCount(m+1,
						    wordIndex*Alphabet::alphabet->factor()+
						    baseIndex));
	    }
	    covar += countW1*countW2 / q1;

	    for (long baseIndex=0; baseIndex < Alphabet::alphabet->size(); baseIndex++) {
	      long curWord=Alphabet::alphabet->factor()*wordIndex+baseIndex;
	      if (c.wordCount(m+1,curWord)>0) {
		covar -= double ( wordCounterW1.wordCount(m+1,curWord) * 
				wordCounterW2.wordCount(m+1,curWord)) / 
		  double (c.wordCount(m+1,curWord));
	      }
	    }
	  }
	  
	}

	long firstIndexW1 = w1.substring(0,m-1);
	long firstIndexW2    = w2.substring(0,m-1);
	double countW2Seq=0.0;
	double countW2W1=0.0;
	double countW1Seq=0.0;
	double countW1W2=0.0;
	for (long baseIndex=0;baseIndex<Alphabet::alphabet->size();baseIndex++) {
	  countW1W2+=wordCounterW2.wordCount(m+1,
				     Alphabet::alphabet->factor()*firstIndexW1+baseIndex);
	  countW1Seq+=c.wordCount(m+1,
				    Alphabet::alphabet->factor()*firstIndexW1+baseIndex);
	  countW2W1+=wordCounterW1.wordCount(m+1,
					       Alphabet::alphabet->factor()*firstIndexW2+
					       baseIndex);
	  countW2Seq+=c.wordCount(m+1,Alphabet::alphabet->factor()*firstIndexW2+baseIndex);
	  
	}
	covar -= countW2W1 /  countW2Seq;
	covar -= countW1W2 /countW1Seq ;

	if (firstIndexW1 == firstIndexW2)
	  covar += 1.0 / countW1Seq;

	covar *= expectW1 * expectW2;

	//	cerr << w1 << "\t" << w2 << "\t" << covar << "\t(1)" << endl;
	
	for (long d=m+1-w1.length() ; d < w1.length()-m ; d++) {
	  auto_ptr<Word> overlapWord(w1.overlaps(w2,d));
	  if (overlapWord.get()) {
	    double overlapexpect=condAsExpect(*overlapWord.get(),m,c);
	    //	    cout << "overlap\t" << d << "\t" << w1 << "\t" << w2 << "\t" << *overlapWord.get() <<"\t" << overlapexpect <<  endl;
	    covar += condAsExpect(*overlapWord.get(),m,c);
	  }
	}

	//	cerr << w1 << "\t" << w2 << "\t" << covar << "\t(2)" << endl;
	
      }
    }
	
    return covar;
  }


  ostream &
  WordEstimator::writeBlockToStream(ostream &os)
  {
    Estimator::writeBlockToStream(os);
    os   << _name << " # " << _algorithm << " " << 0 << endl;
    static const int DEFAULTPRECISION=6;
    os.setf(ios::fixed,ios::floatfield);
    os  << _lmin << " " << _lmax << endl;
    for (short l=_lmin;l<=_lmax;l++) {
      os << endl 
	 << markovOrder(l) << " " << markovOrder(l) << endl;
      WordSequence ws(l);
      long columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _counter->wordCount(l,it->number());
      }
      os << endl;
      os.precision(3);
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _expect[l][it->number()];
      }
      os << endl;
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _var[l][it->number()];
      }
      os << endl;

      os.precision(4);
      columnIndex=-1;
      for(WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	if ((++columnIndex) % NCOLUMNS == 0)
	  os << endl;
	else
	  os << " ";
	os << _stat[l][it->number()];
      }
      os << endl ;
      os.precision(DEFAULTPRECISION);
    }
    return os;
  }


  WordEstimator::~WordEstimator()
  {
  }
};



