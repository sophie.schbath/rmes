/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_RMESPARAMETERS_H
#define RMES_RMESPARAMETERS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <string>
#include <vector>

namespace rmes {

  /**
   * Value Type class for the command line arguments.
   *
   * This class is only designed to grant easy access to the command line
   * arguments once it has been parsed by a ParameterParser instance.
   **/

  class RMESParameters  {
    
  public:
    RMESParameters();
    
    virtual ~RMESParameters() {};
    
    /**
     * Statistical method to be used in computations.
     **/
    inline std::string method() const
    {
      return _method;
    }

    /**
     * Markov order given on the command-line, or -1 of none was given.
     **/
    inline short markovOrder() const
    {
      return _markovOrder;
    }
    
    /**
     * Returns true if the highest possible Markov order was requested
     * on the command-line, false otherwise.
     **/
    inline bool maxOrder() const
    {
      return _maxOrder;
    }

    /*
     * Smallest word length given on the command-line or -1 if none
     * was given.
     */
    inline short minLength() const
    {
      return _minLength;
    }
    
    /**
     * Longest word length given on the command-line or -1 if none was
     * given.
     */
    inline short maxLength() const
    {
      return _maxLength;
    }
    
    /**
     * Name of the file containing the sequence to be processesd given
     * on the command line or empty string if non was given.
     **/
    inline std::string sequenceFile() const
    {
      return _sequenceFile;
    }

    /**
     * Prefix of the outputfiles given on the command-line, or empty
     * string if none was given.
     **/
    inline std::string outputPrefix() const
    {
      return _outputPrefix;
    }
    
    /**
     * Name of the file containing the family data given on the command-line
     * or empty string if none was given.
     **/
    inline std::string familyFile() const
    {
      return _familyFile;
    }
    
    /**
     * Returns true if the DNA alphabet should be used, false otherwise.
     **/
    inline bool DNAAlphabet() const
    {
      return _dnaAlphabet;
    }

    /**
     * Returns true if the amino acid  alphabet should be used, 
     * false otherwise.
     **/
    inline bool AAAlphabet() const
    {
      return _aaAlphabet;
    }



    /**
     * Returns the string given through the --alphabet option.
     **/
    inline std::string alphabetString() const
    {
      return _alphabetString;
    }



    /**
     * Returns if output files must be compressed or not.
     **/
    inline bool compressOutput() const
    {
      return _compressOutput;
    }


    /**
     * Returns the number of phases for which to compute quantitues,
     * 0 means no phases.
     **/
    inline short phases() const
    {
      return _phases;
    }


  protected:
    
    std::string _method;

    short _markovOrder;
    bool _maxOrder;

    short _minLength;
    short _maxLength;
    
    std::string _sequenceFile;
    std::string _outputPrefix;
    
    std::string _familyFile;
    
    long _maxThresh;
    double _precision;


    bool _dnaAlphabet;
    bool _aaAlphabet;
    std::string _alphabetString;

    bool _compressOutput;


    short _phases;

    friend class RMESParameterParser;
    
    
  private:
    RMESParameters(const RMESParameters &) {};
    RMESParameters &operator=(const RMESParameters &) { return *this; };
  };
  
}
#endif
