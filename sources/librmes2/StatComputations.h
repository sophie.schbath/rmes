/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_STATCOMPUTATIONS_H
#define RMES_STATCOMPUTATIONS_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

namespace rmes {

  double
  computeStat(const long, const double, const double);
  
  double
  quantile(const double);

  double
  computeStatPoisson(const long, const double);

  short
  positiveMod(const long, const short);

  double 
  lower_tail(const double lambda, const double a, const long n);

  double 
  upper_tail(const double lambda, const double a, const long n);

  void
  binomial_tails(const long totcount, const long thresh, const double pi0, double *p, double *q);

  double
  binomial_upper_tail(const long totcount, const long thresh, const double pi0);

  double
  binomial_lower_tail(const long totcount, const long thresh, const double pi0);

};
#endif
