/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PhasedMartWordEstimator.h>

#include <iostream>
#include <cmath>
using namespace std;

#include <StatComputations.h>
#include <WordSequence.h>

namespace rmes {

  PhasedMartWordEstimator::PhasedMartWordEstimator(const short lmin,
						     const short lmax,
						     const short nphases) :
    PhasedWordEstimator(lmin,lmax,-1,"Mart",nphases)
  {
    _counter=new PhasedCounter(lmin-1,lmax,nphases);
  }

  void
  PhasedMartWordEstimator::estimate(const Sequence &s)
  {
     this->PhasedWordEstimator::estimate(s);
    _counter->countWords(s);
     for (short l=_lmin; l<=_lmax; l++) {
      WordSequence wseq(l);
      Word prefix(l-1);
      Word suffix(l-1);
      Word radix(l-2);
      for (WordSequence::iterator it=wseq.begin(); it != wseq.end(); ++it) {
	long w=it->number();
	_expect[_nphases][l][w]=0.0;
	_var[_nphases][l][w]=0.0;
	for (short p=0;p<_nphases;p++) {
  	  prefix.setNumber(it->prefix());
 	  long nPrefix=_counter->wordCount(l-1,prefix.number(),positiveMod(p-1,_nphases));
	  suffix.setNumber(it->suffix());
 	  long nSuffix=_counter->wordCount(l-1,suffix.number(),p);
 	  radix.setNumber(prefix.suffix());
 	  long nRadixExtended=0;
 	  for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
 	    long radixExtended=radix.number()*Alphabet::alphabet->factor()+letter;
 	    nRadixExtended+=_counter->wordCount(l-1,radixExtended,p);
 	  }
	  if (nRadixExtended==0)
	    {
	      _expect[p][l][w]=0.0;
	      _var[p][l][w]=0.0;
	      _stat[p][l][w]=0.0;
	    }
	  else
	    {
	      float expect=(1.0*nPrefix*nSuffix)/(1.0*nRadixExtended);
	      float var=expect/(1.0*nRadixExtended*nRadixExtended)*
		(nRadixExtended-nSuffix)*(nRadixExtended-nPrefix);
	      _expect[p][l][w]=expect;
	      if (var<0)
		var=0.0;
	      _var[p][l][w]=var;
	      _stat[p][l][w]=computeStat(_counter->wordCount(l,w,p),expect,var);
	    }
	  _expect[_nphases][l][w]+=_expect[p][l][w];
	  _var[_nphases][l][w]+=_var[p][l][w];
	}
	_stat[_nphases][l][w]=computeStat(_counter->wordCount(l,w,_nphases),
                                          _expect[_nphases][l][w],_var[_nphases][l][w]);
      }
    }
  }



  PhasedMartWordEstimator::~PhasedMartWordEstimator()
  {
    delete _counter;
   }

};
