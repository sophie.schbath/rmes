/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <CompoundPoissonWordResultSet.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <ios>
#include <iostream>

using namespace std;

#include <WordSequence.h>

namespace rmes {

  CompoundPoissonWordResultSet::CompoundPoissonWordResultSet(const short minSize, 
					 const short maxSize,
					 const float minThresh,
					 const float maxThresh) :
    WordResultSet(minSize,maxSize,minThresh,maxThresh,"Compound Poisson")
  {
	  vector<HeaderStruct>::iterator it=find(_headers.begin(),_headers.end(),HeaderStruct("score",0));
	  _headers.insert(it,HeaderStruct("A",12));
	  it=find(_headers.begin(),_headers.end(),HeaderStruct("A",0));
	  _headers.insert(it,HeaderStruct("expect_p",10));
  }

  void
  CompoundPoissonWordResultSet::resize(const short minSize, const short maxSize) {
	  WordResultSet::resize(minSize,maxSize);
	  _expect_p.resize(maxSize+1);
	  _Aparam.resize(maxSize+1);
	  for(short size=_minSize; size<=_maxSize; size++) {
		  _expect_p[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
		  _Aparam[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
	  }
  }
  
  istream &
  CompoundPoissonWordResultSet::readDataBlock(istream &is, const short size,
				    const string &h)
  {
    if (size >=_minSize && size <=_maxSize && h == "expect_p") {
      for (long index=0; index<_expect_p[size].size(); index++)
	_expect_p[size][index]=readFloatValue(is);
    } else if (size >=_minSize && size <=_maxSize && h == "A") {
      for (long index=0; index<_Aparam[size].size(); index++)
	_Aparam[size][index]=readFloatValue(is);
    } else
      WordResultSet::readDataBlock(is,size,h);

    return is;
  }

  ostream &
  CompoundPoissonWordResultSet::writeParameter(std::ostream &os, const short size,
				     const HeaderStruct &h, const long index) const
  {
    if (h._name == "expect_p")
      os << ResultSet::FIELDSEPARATOR << setprecision(3) <<  fixed << setw(h._width) << right << _expect_p[size][index];
    else if (h._name == "A")
      os << ResultSet::FIELDSEPARATOR << setprecision(5) <<  fixed << setw(h._width) << right << _Aparam[size][index];
    else
      WordResultSet::writeParameter(os,size,h,index);
    return os;
  }

  CompoundPoissonWordResultSet::~CompoundPoissonWordResultSet()
  {
  }
};
