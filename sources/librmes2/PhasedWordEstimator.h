/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_PHASEDWORDESTIMATOR_H
#define RMES_PHASEDWORDESTIMATOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <MarkovEstimator.h>
#include <PhasedCounter.h>
#include <PhasedEstimator.h>
#include <PhasedWord.h>
#include <Sequence.h>

namespace rmes {

  class PhasedWordEstimator : public PhasedEstimator, 
			public MarkovEstimator {
  public :

    virtual ~PhasedWordEstimator();

    virtual void estimate(const Sequence &);
    virtual void countWords(const Sequence &);

    virtual void setOutputMode(const OutputMode);

    virtual short minLength() const;
    virtual short maxLength() const;
    
    virtual long wordCount(const short, const long, const short);
    virtual long numWords(const short);


    virtual std::ostream &writeBlockToStream(std::ostream &);

  protected :
    PhasedWordEstimator(const short, const short, const short,
		  const std::string &, const short);

    virtual long *wordCounts(const short,const short);

    virtual double condAsExpect(const PhasedWord &, const short , const PhasedCounter &);
    
    virtual double condAsVar(const PhasedWord &, const short, const PhasedCounter &, const PhasedCounter &, const double );
    
    virtual double condAsCoVar(const PhasedWord & , const PhasedWord &,
			       const short , 
    			       const PhasedCounter &,
			       const PhasedCounter &,
			       const double , const double );

    short _lmin;
    short _lmax;

    PhasedCounter *_counter;

  private :
    PhasedWordEstimator();
    PhasedWordEstimator(const PhasedWordEstimator &);
    PhasedWordEstimator &operator=(const PhasedWordEstimator &);

  };
};


#endif
