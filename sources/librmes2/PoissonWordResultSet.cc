/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <PoissonWordResultSet.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <ios>
#include <iostream>

using namespace std;

#include <WordSequence.h>

namespace rmes {

  PoissonWordResultSet::PoissonWordResultSet(const short minSize, 
					 const short maxSize,
					 const float minThresh,
					 const float maxThresh) :
    WordResultSet(minSize,maxSize,minThresh,maxThresh,"Poisson")
  {
	  vector<HeaderStruct>::iterator it=find(_headers.begin(),_headers.end(),HeaderStruct("score",0));
	  assert(it != _headers.end());
	  _headers.insert(it,HeaderStruct("sigma2",10));
	  it=find(_headers.begin(),_headers.end(),HeaderStruct("count",0));
	  it->_name="count_p";
	  it=find(_headers.begin(),_headers.end(),HeaderStruct("expect",0));
	  it->_name="expect_p";
  }


  void
  PoissonWordResultSet::resize(const short minSize, const short maxSize) {
	  WordResultSet::resize(minSize,maxSize);
	  _var.resize(maxSize+1);
	  for(short size=_minSize; size<=_maxSize; size++)
		  _var[size].resize((long)pow(Alphabet::alphabet->size(),(float)size));
  }
  
  istream &
  PoissonWordResultSet::readDataBlock(istream &is, const short size,
				    const string &h)
  {
    if (size >=_minSize && size <=_maxSize && h == "count_p") {
      for (long index=0; index<_counts[size].size(); index++)
	is >> _counts[size][index];
    } else if (size >=_minSize && size <=_maxSize && h == "expect_p") {
      for (long index=0; index<_expect[size].size(); index++)
	_expect[size][index]=readFloatValue(is);
    } else if (size >=_minSize && size <=_maxSize && h == "sigma2") {
      for (long index=0; index<_var[size].size(); index++)
	_var[size][index]=readFloatValue(is);
    } else
      WordResultSet::readDataBlock(is,size,h);

    return is;
  }

  ostream &
  PoissonWordResultSet::writeParameter(std::ostream &os, const short size,
				     const HeaderStruct &h, const long index) const
  {
    if (h._name == "count_p")
      os << ResultSet::FIELDSEPARATOR << setprecision(3) <<  fixed << setw(h._width) << right << _counts[size][index];
    else if (h._name == "expect_p")
      os << ResultSet::FIELDSEPARATOR << setprecision(3) <<  fixed << setw(h._width) << right << _expect[size][index];
    else if (h._name == "sigma2")
      os << ResultSet::FIELDSEPARATOR << setprecision(3) <<  fixed << setw(h._width) << right << _var[size][index];
    else
      WordResultSet::writeParameter(os,size,h,index);
    return os;
  }

  PoissonWordResultSet::~PoissonWordResultSet()
  {
  }
};
