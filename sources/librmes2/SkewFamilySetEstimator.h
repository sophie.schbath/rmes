/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_SKEWFAMILYSETESTIMATOR_H
#define RMES_SKEWFAMILYSETESTIMATOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>

#include <Counter.h>
#include <GaussFamilySetEstimator.h>
#include <SkewEstimator.h>

namespace rmes {

  class SkewFamilySetEstimator : public GaussFamilySetEstimator,
				 public SkewEstimator 
  {
  public:
    SkewFamilySetEstimator(const FamilySet &fs, const short m);
    virtual ~SkewFamilySetEstimator();
    virtual void estimate(const Sequence &s);
  protected :

    virtual std::ostream &writeSkewToStream(std::ostream &);

  private :
    std::vector<std::vector<long> > _conjcounts;

  };

};

#endif
