/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <FormatParameterParser.h>

#include <cmath>
#include <cstdlib>
#include <exception>
#include <vector>
using namespace std;

using namespace TCLAP;

namespace rmes {

  FormatParameterParser::FormatParameterParser(const string &progname,
				   const string &version)
    
  {
    
    _cmdLine=new CmdLine(progname,' ',version);
    
    _minLengthArg=new ValueArg<short>("i","hmin","smallest word size.",false,-1,
				    "int");
    
    _maxLengthArg=new ValueArg<short>("a","hmax","largest word size.",false,-1,
				    "int");
    
    
    _wordLengthArg=new ValueArg<short>("l","length",
				     "count all words of this length only",
				     false,-1,"int");

    _maxThreshArg=new ValueArg<float>("","tmax","Max. Threshold",
				     false,100,"float value");

    _minThreshArg=new ValueArg<float>("","tmin","Min. Threshold",
				     false,100,"float value");


    _cmdLine->add(_minThreshArg);
    _cmdLine->add(_maxThreshArg);
    _cmdLine->add(_wordLengthArg);
    _cmdLine->add(_maxLengthArg);
    _cmdLine->add(_minLengthArg);
    
  }

  void
  FormatParameterParser::parse(int argc, char **argv, FormatParameters *params) 
  {
    
    _cmdLine->parse(argc,argv);
    
    if (_minLengthArg->isSet() && _maxLengthArg->isSet()) {
      params->_minLength=_minLengthArg->getValue();
      params->_maxLength=_maxLengthArg->getValue();
      if (params->_minLength>params->_maxLength)
	      throw FormatParameterParserException("Option -a (--hmax) must be equal or greater than option -i (--hmin).");
      if (_wordLengthArg->isSet()) 
	throw FormatParameterParserException("Option -l (--wordlength) is not compatible with options -i (--hmin) and -a (--hmax).");
    } else if (_minLengthArg->isSet() || _maxLengthArg->isSet()) {
      throw FormatParameterParserException("Options -i (--hmin) and -a (--hmax) must both be set or both me ommitted.");
    } else if (_wordLengthArg->isSet()) {
      params->_minLength=_wordLengthArg->getValue();
      params->_maxLength=_wordLengthArg->getValue();
    } 

    if (_minThreshArg->isSet()) {
	params->_minThresh=-fabs(_minThreshArg->getValue());
	params->_maxThresh=-params->_minThresh;
    }

    if (_maxThreshArg->isSet()) {
	params->_maxThresh=fabs(_maxThreshArg->getValue());
	if (!_minThreshArg->isSet())
	  params->_minThresh=-params->_maxThresh;
    }

  }

  FormatParameterParser::~FormatParameterParser() {
    delete _minLengthArg;
    delete _maxLengthArg;
    delete _wordLengthArg;
    delete _minThreshArg;
    delete _maxThreshArg;
    delete _cmdLine;
  }

};
