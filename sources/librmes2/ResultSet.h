/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_RESULTSET_H
#define RMES_RESULTSET_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <vector>


typedef std::vector<long> longvect;
typedef std::vector<float> floatvect;


namespace rmes {

  struct IndexedStats {
    IndexedStats(const long index =-1, const float stat = -1e38) : _index(index), _stat(stat) {};
    long _index;
    float _stat;
  };

  inline bool
	  CompareIndexedStats (const IndexedStats &i1, const IndexedStats &i2) {
		  return (i1._stat < i2._stat);
		  
	  }
  typedef std::vector<IndexedStats> IndexedStatsvect;

  struct HeaderStruct {
	  HeaderStruct(const std::string name, const short width) : _name(name), _width(width) {};
	  std::string _name;
	  short _width;
  };
 
  inline bool
	 operator==(const HeaderStruct &i1, const HeaderStruct &i2) {
		return (i1._name == i2._name);
	 }

  class ResultSet {
  public:
    virtual ~ResultSet();
    inline void setDataSetName(const std::string &dsn) { _dataSetName = dsn;} ;

  protected:


    static const std::string RANKSEPARATOR;
    static const std::string FIELDSEPARATOR;
    
    ResultSet(const short, const short, const float, const float, const std::string &);

    virtual void resize(const short, const short) = 0;

    virtual float readFloatValue(std::istream &);

    virtual std::ostream & writeToStream(std::ostream &) const ;
    virtual std::istream & readFromStream(std::istream &)  =0;
   
    virtual std::ostream &writeParameter(std::ostream &, const short ,
		       	const HeaderStruct &, const long) const;	
    short _minSize;
    short _maxSize;
    float _minThresh;
    float _maxThresh;

    mutable std::vector<HeaderStruct> _headers;
    std::vector<longvect> _counts;
    std::vector<floatvect> _expect;
    std::vector<IndexedStatsvect> _stat;

    std::string _algorithm;
    std::vector<std::string> _markovOrders;
    std::string _dataSetName;

    friend std::ostream &operator<<(std::ostream &, const ResultSet &);
    friend std::istream &operator>>(std::istream &, ResultSet &);

  };


  std::ostream & operator<<(std::ostream &, const ResultSet &);
  std::istream & operator>>(std::istream &, ResultSet &);

};

#endif
