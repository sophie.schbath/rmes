/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <RMESParameterParser.h>

#include <exception>
#include <cstdlib>
#include <vector>
using namespace std;

using namespace TCLAP;

namespace rmes {

  RMESParameterParser::RMESParameterParser(const string &progname,
				   const string &version)
    
  {
    
    _cmdLine=new CmdLine(progname,' ',version);
    
    vector<Arg *> methodXorVect;
    
    _gaussMethodArg=new SwitchArg("","gauss","Use Gaussian method",true);
    methodXorVect.push_back(_gaussMethodArg);
    _poissonMethodArg=new SwitchArg("","poisson","Use Poisson method",true);
    methodXorVect.push_back(_poissonMethodArg);
    _compoundPoissonMethodArg=new SwitchArg("","compoundpoisson","Use compound Poisson method",true);
    methodXorVect.push_back(_compoundPoissonMethodArg);
    _skewMethodArg=new SwitchArg("","skew","Compute skew with Gaussian method",true);
    methodXorVect.push_back(_skewMethodArg);

    _markovOrderArg=new ValueArg<short>("m","markov_order",
				      "Markov order of the model.",
				      false,-1,"int");
    _maxOrderArg=new SwitchArg("","max","Use highest possible Markov order",false);

    _minLengthArg=new ValueArg<short>("i","lmin","smallest word size.",false,-1,
				    "int");
    
    _maxLengthArg=new ValueArg<short>("a","lmax","largest word size.",false,-1,
				    "int");
    
    
    _wordLengthArg=new ValueArg<short>("l","length",
				     "count all words of this length only",
				     false,-1,"int");
    _sequenceFileArg=new ValueArg<string>("s","seq",
					  "sequence file in FASTA or GenBank format.",
					  false,"","filename");
    
    _outputPrefixArg=new ValueArg<string>("o","out","prefix for output files.",
					  true,"","string");
    
    _familyFileArg=new ValueArg<string>("f","fam","family file.",
					false,"","filename");

    _phasesArg=new ValueArg<short>("","phases","number of phases.",
					false,0,"integer");


    _dnaAlphabetArg=new SwitchArg("","dna","Use nucleotide alphabet",false);

    _aaAlphabetArg=new SwitchArg("","aa","Use amino acid alphabet",false);

    _alphabetStringArg=new ValueArg<string>("","alphabet","Specify a string to be used as alphabet for the sequences",false,"","character string");

#ifdef HAVE_ZLIB_H
    _compressOutputArg=new SwitchArg("z","compress","Compress output files.",false);

    _cmdLine->add(_compressOutputArg);
#endif

    _cmdLine->add(_alphabetStringArg);
    _cmdLine->add(_aaAlphabetArg);
    _cmdLine->add(_dnaAlphabetArg);
    _cmdLine->add(_phasesArg);
    _cmdLine->add(_familyFileArg);
    _cmdLine->add(_maxOrderArg);
    _cmdLine->add(_markovOrderArg);
    _cmdLine->add(_wordLengthArg);
    _cmdLine->add(_maxLengthArg);
    _cmdLine->add(_minLengthArg);
    _cmdLine->add(_outputPrefixArg);
    _cmdLine->add(_sequenceFileArg);
    _cmdLine->xorAdd(methodXorVect);

    
  }

  void
  RMESParameterParser::parse(int argc, char **argv, RMESParameters *params) 
  {
    
    _cmdLine->parse(argc,argv);
    
    if (_gaussMethodArg->isSet())
      params->_method="Gauss";
    if (_poissonMethodArg->isSet())
      params->_method="Poisson";
    if (_compoundPoissonMethodArg->isSet())
      params->_method="compound Poisson";

    if (_skewMethodArg->isSet()) {
      params->_method="skew";
      if (_aaAlphabetArg->isSet() || _alphabetStringArg->isSet())
	throw RMESParameterParserException("Skew computation is only valid with DNA alphabet.");
    }


    if (!_sequenceFileArg->isSet()) {
      throw RMESParameterParserException("A sequence file must be given (-s or --seq options).");
    } 
    
    params->_sequenceFile=_sequenceFileArg->getValue();

    params->_outputPrefix=_outputPrefixArg->getValue();
    
    if (_dnaAlphabetArg->isSet() && _aaAlphabetArg->isSet())
      throw RMESParameterParserException("Options --dna and --aa are mutually exclusive.");
    params->_dnaAlphabet=true;
    if (_aaAlphabetArg->isSet())
      params->_aaAlphabet=true;
    
    if (_alphabetStringArg->isSet()) {
      if (_dnaAlphabetArg->isSet() || _aaAlphabetArg->isSet())
	throw RMESParameterParserException("Option --alphabet exludes options -dna and --aa.");
      params->_dnaAlphabet=false;
      params->_aaAlphabet=false;
      params->_alphabetString=_alphabetStringArg->getValue();
    }

    if (_markovOrderArg->isSet() && _maxOrderArg->isSet())
      throw RMESParameterParserException("Options -m and --max are mutually exclusive.");

    if (_markovOrderArg->isSet())
      params->_markovOrder=_markovOrderArg->getValue();
    else if (_maxOrderArg->isSet()) {
      params->_maxOrder=true;
      /**     
	      if (params->_method != "Poisson")
	      params->_markovOrder=1;
      **/
    }

    if (_minLengthArg->isSet() && _maxLengthArg->isSet()) {
      params->_minLength=_minLengthArg->getValue();
      params->_maxLength=_maxLengthArg->getValue();
      if (_wordLengthArg->isSet()) 
	throw RMESParameterParserException("Option -l (--wordlength) is not compatible with options -i (--hmin) and -a (--hmax).");
      if (_familyFileArg->isSet())
	throw RMESParameterParserException("Option -f (--fam) is not compatible with options -i (--hmin) and -a (--hmax).");
    } else if (_minLengthArg->isSet() || _maxLengthArg->isSet()) {
      throw RMESParameterParserException("Options -i (--hmin) and -a (--hmax) must both be set or both me ommitted.");
    } else if (_wordLengthArg->isSet()) {
      params->_minLength=_wordLengthArg->getValue();
      params->_maxLength=_wordLengthArg->getValue();
      if (_familyFileArg->isSet())
	throw RMESParameterParserException("Option -f (--fam) and -l (--wordlength) are incompatible.");
    } else if (_familyFileArg->isSet()) {
      params->_familyFile=_familyFileArg->getValue();    
    } else {
      throw RMESParameterParserException("Either word length information must be given (options -i (--hmin) and -a (--hmax) or -l (--wordlength), or a filename with family information must be specified with option -f (--fam).");
      
    }
    
    if (_phasesArg->isSet()) {
      if (_gaussMethodArg->isSet()) 
	params->_phases=_phasesArg->getValue();
      else
	throw RMESParameterParserException("Option --phases is only usable with --gauss method.");
    }

#ifdef HAVE_ZLIB_H
    if (_compressOutputArg->isSet())
      params->_compressOutput=true;
#endif

  }

  RMESParameterParser::~RMESParameterParser() {
    delete _gaussMethodArg;
    delete _poissonMethodArg;
    delete _compoundPoissonMethodArg;
    delete _skewMethodArg;
    delete _markovOrderArg;
    delete _maxOrderArg;
    delete _minLengthArg;
    delete _maxLengthArg;
    delete _wordLengthArg;
    delete _sequenceFileArg;
    delete _outputPrefixArg;
    delete _familyFileArg;
    delete _phasesArg;
    delete _dnaAlphabetArg;
    delete _aaAlphabetArg;
    delete _alphabetStringArg;
#ifdef HAVE_ZLIB_H
    delete _compressOutputArg;
#endif
    delete _cmdLine;
  }

};
