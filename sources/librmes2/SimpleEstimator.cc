/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <SimpleEstimator.h>

#include <algorithm>
#include <iostream>

using namespace std;

#include <WordSequence.h>

namespace rmes {
  
  SimpleEstimator::SimpleEstimator(const string &algorithm) : 
    Estimator(algorithm)
  {
  }

  void
  SimpleEstimator::estimate(const Sequence &s)
  {
    _name=s.name();
  }

  double *
  SimpleEstimator::expectations(const short l)
  {
    return (&(_expect[l][0]));
  }

  double
  SimpleEstimator::expectation(const short l, const long w)
  {
    return _expect[l][w];
  }

  double *
  SimpleEstimator::variances(const short l)
  {
    return (&(_var[l][0]));
  }

  double
  SimpleEstimator::variance(const short l, const long w)
  {
    return _var[l][w];
  }


  double *
  SimpleEstimator::statistics(const short l)
  {
    return (&(_stat[l][0]));
  }

  double
  SimpleEstimator::statistic(const short l, const long w)
  {
    return _stat[l][w];
  }

  ostream &
  SimpleEstimator::writeBlockToStream(ostream &os)
  {
    WordSequence ws(1);
    os << "# " << PACKAGE << "-" << VERSION << endl
       << "# Alphabet = ";
    copy(ws.begin(),ws.end(),ostream_iterator<Word>(os," "));
    os << endl;
    os   << _name << " # " << _algorithm << " " << 0 << endl;
        return os;
  }

  SimpleEstimator::~SimpleEstimator()
  {
  }

  ostream &
  operator<<(ostream &os, const SimpleEstimator &ce)
  {
    SimpleEstimator &e=const_cast<SimpleEstimator &>(ce);
    switch (e._outputMode) {
    case Estimator::BLOCK :
      e.writeBlockToStream(os);
      break;
    case Estimator::ALPHA :
      e.writeAlphaToStream(os);
      break;
    }
    return os;
  }
  
};
