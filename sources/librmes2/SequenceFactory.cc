/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <vector>
using namespace std;

#include <Alphabet.h>
#include <SequenceFactory.h>



namespace rmes {

  SequenceFactory::SequenceFactory(const string &fileName) : 
    _seqStream(fileName.c_str()),
    _hasMoreSequences(false)
  {
  }

  bool 
  SequenceFactory::hasMoreSequences() 
  {
    string header;

    if (!_seqStream.good())
	throw SequenceFactoryException("Unable to read sequence file.");
    getline(_seqStream,header);
    
    if (header[0]=='>') {
      buildFromFASTA(header);
    } else  if (header.find("LOCUS")==0) {
      buildFromGenBank(header);
    } else {
      throw SequenceFactoryException("Sequence is not in FASTA nor Genbank format.");
    }
    return _hasMoreSequences;
  }


  Sequence 
  SequenceFactory::nextSequence()
  {
    _hasMoreSequences=false;
    return _curSequence;
  }


  void
  SequenceFactory::buildFromFASTA(const string &header) 
  {
    _curSequence._name=header.substr(1);
    _curSequence._seq.clear();
    for_each(istream_iterator<char>(_seqStream),istream_iterator<char>(),
	     CharToBaseMapper(&_curSequence._seq));
    _hasMoreSequences=true;
  }

  void
  SequenceFactory::buildFromGenBank(const string &header) 
  {
    const string DEFINITION="DEFINITION ";
    const string ORIGIN="ORIGIN";
    const int    HEADERCOLUMNS=10;

    _curSequence._name.clear();
    _curSequence._seq.clear();

    string line;
    
    /*
     * Get definition.
     */
    bool definitionFound=false;
    while (!definitionFound && !_seqStream.eof()) {
      getline(_seqStream,line);
      if (line.find(DEFINITION) == 0) {
	definitionFound=true;
	_curSequence._name=line.substr(DEFINITION.length());
      }
    }
    if (!definitionFound) {
      cerr << "No definition found !" << endl;
      return;
    }
    
    /*
     * Skip to origin.
     */
    bool originFound=false;
    while (!originFound && !_seqStream.eof()) {
      getline(_seqStream,line);
      if (line.find(ORIGIN) == 0) {
	originFound=true;
      }
    }
    if (!originFound)
      return;

    /*
     * Read sequence lines.
     */
    while (line!="//" && !_seqStream.eof()) {
      getline(_seqStream,line);
      if (line != "//") {
	string seqstring=line.substr(HEADERCOLUMNS);
	for_each(seqstring.begin(),seqstring.end(),
		 CharToBaseMapper(&_curSequence._seq));
      }
    }
    if (_seqStream.eof())
      return;

    _hasMoreSequences=true;

  }

  SequenceFactory::~SequenceFactory() 
  {
    _seqStream.close();
  }
  
}
