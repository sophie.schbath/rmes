/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#ifndef RMES_PHASEDESTIMATOR_H
#define RMES_PHASEDESTIMATOR_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string>
#include <vector>

#include <Estimator.h>
#include <Sequence.h>

typedef std::vector<double> vecdouble;
typedef std::vector<vecdouble> matdouble;

namespace rmes {

  class PhasedEstimator : public Estimator {
  public :

    virtual ~PhasedEstimator();

    virtual void estimate(const Sequence &);

    virtual double expectation(const short, const long, const short);
    virtual double variance(const short, const long, const short);
    virtual double statistic(const short, const long, const short);


    virtual short nPhases() const;
    virtual void setCurrentPhase(const short);

  protected :
    PhasedEstimator(const std::string &, const short);

    virtual double *expectations(const short,const short);
    virtual double *variances(const short, const short);
    virtual double *statistics(const short, const short);


    virtual std::ostream &writeBlockToStream(std::ostream &);

    std::vector<matdouble> _expect;
    std::vector<matdouble> _var;
    std::vector<matdouble> _stat;

    short _nphases;
    short _curphase;

  private:    
    PhasedEstimator(const PhasedEstimator &);
    PhasedEstimator &operator=(const PhasedEstimator &);

    friend std::ostream & operator<<(std::ostream &, const PhasedEstimator &);

  };
};


#endif
