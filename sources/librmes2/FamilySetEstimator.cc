/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <FamilySetEstimator.h>

#include <iomanip>
using namespace std;

namespace rmes {

  FamilySetEstimator::FamilySetEstimator(const string &n, const FamilySet &f,
					 const short m) :
    WordEstimator(f.wordLength(),f.wordLength(),m,n+"_familles"), 
    _methodName(n+"_familles"), _familySet(f)
  {
    _expect.resize(f.wordLength()+1);
    _stat.resize(f.wordLength()+1);
    _var.resize(f.wordLength()+1);
    _counts.resize(f.wordLength()+1);
    _expect[f.wordLength()].resize(f.numberOfFamilies());
    _stat[f.wordLength()].resize(f.numberOfFamilies());
    _var[f.wordLength()].resize(f.numberOfFamilies());
    _counts[f.wordLength()].resize(f.numberOfFamilies());

    _counter=new Counter(f.wordLength(),f.wordLength());
    _counterPlusOne=new Counter((m==-1)?(f.wordLength()-1):(m+1),
				(m==-1)?(f.wordLength()-1):(m+1));
    
  }

  void
  FamilySetEstimator::estimate(const Sequence &s)
  {
    WordEstimator::estimate(s);
    _counter->countWords(s);
    _counterPlusOne->countWords(s);
  }

  ostream &
  FamilySetEstimator::writeBlockToStream(ostream &os) {
    Estimator::writeBlockToStream(os);
    os   << _name << " # " << _algorithm << " " << 0 << endl;
    os.setf(ios::fixed,ios::floatfield);
    os << _familySet.wordLength()  << " " << _familySet.wordLength() << endl
       << _familySet.numberOfFamilies() << " " << _familySet.wordsPerFamily() << endl
       << _familySet.fileName() << endl
       << _familySet.wordLength() << endl
        << markovOrder(_familySet.wordLength()) 
       << " " << markovOrder(_familySet.wordLength()) << endl;

    for (long familyIndex=0; familyIndex < _familySet.numberOfFamilies() ;
	 familyIndex++) {
      if (familyIndex % NCOLUMNS == 0)
	os << endl;
      os << _familySet[familyIndex].name() << " " 
	 << _counts[_familySet.wordLength()][familyIndex] << " " 
	 << setprecision(3) 
	 << _expect[_familySet.wordLength()][familyIndex] << " "
	 << setprecision(3) 
	 << _var[_familySet.wordLength()][familyIndex] << " "
	 << setprecision(4) 
	 << _stat[_familySet.wordLength()][familyIndex] << " " << endl;
    }
    return os; 
  }

  ostream &
  FamilySetEstimator::writeAlphaToStream(ostream &os) {
    return os;
  }



  FamilySetEstimator::~FamilySetEstimator()
  {
    delete _counter;
    delete _counterPlusOne;
  }

};
