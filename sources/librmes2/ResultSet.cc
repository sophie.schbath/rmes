/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <ResultSet.h>

#include <cmath>
#include <cstdlib>
#include <ios>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <sstream>

using namespace std;

#include <Alphabet.h>

namespace rmes {

	const string ResultSet::RANKSEPARATOR("....................");
        const string ResultSet::FIELDSEPARATOR(" | ");

  ResultSet::ResultSet(const short minSize, const short maxSize, const float minThresh, const float maxThresh, const string &algorithm) :
    _minSize(minSize), _maxSize(maxSize), _minThresh(minThresh), _maxThresh(maxThresh), _algorithm(algorithm)
  {
    _headers.push_back(HeaderStruct("count",10));
    _headers.push_back(HeaderStruct("expect",10));
    _headers.push_back(HeaderStruct("score",10));
  }

  ostream &
  ResultSet::writeParameter(ostream &os, const short s,
				const HeaderStruct &h, const long index) const
  {
    if (h._name=="count") 
      os << FIELDSEPARATOR << setprecision(0) << fixed << setw(h._width) << right << _counts[s][index];

    if (h._name=="expect")
      os << FIELDSEPARATOR << setprecision(3) << fixed << setw(h._width) << right << _expect[s][index];

    if (h._name=="score")
      os << FIELDSEPARATOR << setprecision(4) << fixed << setw(h._width) << right << _stat[s][index]._stat;

    return os;
  }

  ostream &
  ResultSet::writeToStream(ostream &os) const 
  {
    for (short size=_minSize; size<=_maxSize;size++) {
	    os << "Results from algorithm  : " << _algorithm << endl;
	    os << "Markov Order            : " << _markovOrders[size] << endl;
	    os << "Word Size               : " << size << endl; 
	    os << "Data Set                : " << _dataSetName << endl;
	    os << "Stat Value Interval     : ] -Inf, " << _minThresh << " ] U [ " << _maxThresh << ", +Inf ]" << endl;
	    string headerline;
	    ostringstream headerlinestream(headerline);
	    for (vector<HeaderStruct>::iterator hit=_headers.begin(); hit != _headers.end(); hit++) {
		    if ((hit->_name=="word" || hit->_name == "family") && hit->_width < size)
			    	hit->_width=size;
	    	headerlinestream << setw(hit->_width) << hit->_name << FIELDSEPARATOR;
	    }
	    headerlinestream << setw(6) << right << "rank";
	    string dashes(headerlinestream.str().length(),'-');
	    string spaces((headerlinestream.str().length()-RANKSEPARATOR.length())/2,' ');
	    os << dashes << endl << headerlinestream.str() << endl << dashes << endl;
	    bool separatorWritten=false;
	    for (long index=0; index<_stat[size].size(); index++) {
	      if (_stat[size][index]._stat<=_minThresh || _stat[size][index]._stat>=_maxThresh) {
		      if (index> 1 && _stat[size][index-1]._stat <= _minThresh && _stat[size][index]._stat >=_maxThresh) {
	      		os <<  spaces << RANKSEPARATOR << endl;
			separatorWritten=true;      
		      }
		      long wordIndex=_stat[size][index]._index;
		      for (vector<HeaderStruct>::const_iterator hit=_headers.begin();  
				      hit !=_headers.end(); hit++) {
			      if (hit->_name != "score" )
				      writeParameter(os,size,*hit,wordIndex);
			      else
				      writeParameter(os,size,*hit,index);
		      }
		      os << FIELDSEPARATOR << setprecision(0) << right << fixed << setw(6) << index+1 << endl;
	      } else if (separatorWritten == false) {
	      	os <<  spaces << RANKSEPARATOR << endl;
		separatorWritten=true;
	      }
	      
	    }
	    os << dashes << endl;
    }
    return os;
  }
  
  float ResultSet::readFloatValue(istream &is)
  {
    string numberString;

    is >> numberString;

    float res=atof(numberString.c_str());

    if (numberString == "inf" || numberString == "+inf")
      res=numeric_limits<float>::infinity();

    if (numberString == "-inf")
      res=-1.0*numeric_limits<float>::infinity();

    return res;
    
  }

  ostream &
  operator<<(ostream &os, const ResultSet &r)
  {
    return r.writeToStream(os);
  }

  istream &
  operator>>(istream &is, ResultSet &r)
  {
    return r.readFromStream(is);
  }

  ResultSet::~ResultSet()
  {
  }


};
