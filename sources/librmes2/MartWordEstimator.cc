/**
    Copyright 2006, 2007, 2008, 2009 INRA
 
    Authors: Sophie Schbath & Mark Hoebeke.

    This file is part of RMES (IDDN.FR.001.440008.000.R.C.2009.000.31235)

    RMES is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    RMES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RMES; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
**/

#include <MartWordEstimator.h>

#include <cmath>
using namespace std;

#include <StatComputations.h>
#include <Word.h>
#include <WordSequence.h>
namespace rmes {
  
  MartWordEstimator::MartWordEstimator(const short lmin, const short lmax) : 
    WordEstimator(lmin, lmax, -1, "Mart") 
  {
    /**
       A priori le minCountWordSize vaut lmin-1 donc pas de test a faire 
       et il est inutile de declarer les deux variables ci-dessous.
       (Sophie)
    **/
   short minCountWordSize=lmin>2?lmin-2:1;
    short maxCountWordSize=lmax+1;
    
    _counter=new Counter(minCountWordSize,maxCountWordSize);
    
    
  }
  
  
  void
  MartWordEstimator::estimate(const Sequence &s)
  {
    this->WordEstimator::estimate(s);
    
    _counter->countWords(s);

    for (int l=_lmin; l<=_lmax; l++) {
      WordSequence ws(l);
      Word prefix(l-1);
      Word suffix(l-1);
      Word radix(l-2);
      for (WordSequence::iterator it=ws.begin(); it != ws.end(); it++) {
	prefix.setNumber(it->prefix());
	long nPrefix=_counter->wordCount(l-1,prefix.number());
	suffix.setNumber(it->suffix());
	long nSuffix=_counter->wordCount(l-1,suffix.number());
	radix.setNumber(prefix.suffix());
	long nRadixExtended=0;
	for (int letter=0;letter<Alphabet::alphabet->size();letter++) {
	  long radixExtended=radix.number()*Alphabet::alphabet->factor()+letter;
	  nRadixExtended+=_counter->wordCount(l-1,radixExtended);
	}
	float expect=(1.0*nPrefix*nSuffix)/(1.0*nRadixExtended);
	float var=expect/(1.0*nRadixExtended*nRadixExtended)*
	  (nRadixExtended-nSuffix)*(nRadixExtended-nPrefix);
	_expect[l][it->number()]=expect;
	_var[l][it->number()]=var;
	_stat[l][it->number()]=(_counter->wordCount(l,it->number())-
				expect)/sqrt(var);
      }
    }
  }
  
  
  ostream &
  MartWordEstimator::writeAlphaToStream(ostream &os)
  {
    return os;
  }
  
  MartWordEstimator::~MartWordEstimator()
  {
    delete _counter;
    
  }
};
